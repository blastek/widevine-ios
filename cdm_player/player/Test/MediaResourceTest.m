#import <OCMock/OCMock.h>
#import <XCTest/XCTest.h>

#import "CdmPlayerErrors.h"
#import "CdmWrapper.h"
#import "Downloader.h"
#import "Logging.h"
#import "MediaResource.h"

float const kWaitForMediaResourceTimeout = 0.2;
static NSString *const kThumbnailURL = @"www.thumbnail.com/picture.png";
static NSString *const kMediaName = @"exciting movie";
static NSString *const kRealManifestName = @"tears_cenc_small";
static NSString *const kRealManifestContentAudio = @"tears_audio_eng";
static NSString *const kRealManifestContentVideo = @"tears_h264_baseline_240p_800";
static NSString *const kCancelToken = @"cancel token";

@interface MediaResourceTest : XCTestCase
@property MediaResource *resource;
@property DDTTYLogger *logger;
@property int numTimesDownloadStatusChanged;
@property int numTimesDownloadCanceled;
@property NSMutableArray<NSURL *> *filesDownloaded;
@property NSArray *mocks;
@end

@implementation MediaResourceTest

- (void)setUp {
  _logger = [DDTTYLogger sharedInstance];
  [DDLog addLogger:_logger];
  self.numTimesDownloadStatusChanged = 0;
  self.numTimesDownloadCanceled = 0;
  self.filesDownloaded = [[NSMutableArray<NSURL *> alloc] init];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(downloadChangedSelector:)
                                               name:kDownloadedStatusChangedNotification
                                             object:nil];
}

- (void)tearDown {
  [DDLog removeLogger:_logger];

  // Clear out mocks.
  for (id mock in self.mocks) {
    OCMVerifyAll(mock);
    [mock stopMocking];
  }
  self.mocks = nil;
}

- (void)testNeedsJson {
  XCTAssertNil([[MediaResource alloc] initWithJson:nil]);
}

- (void)testInit {
  NSString *mediaFile = @"manifestForInit.mpd";
  NSURL *expectedURL =
    [NSURL URLWithString:[NSString stringWithFormat:@"www.test.com/%@", mediaFile]];
  [self makeResourceWithMediaFile:mediaFile];
  XCTAssertNotNil(self.resource);
  XCTAssertEqualObjects(self.resource.thumbnailURL, [NSURL URLWithString:kThumbnailURL]);
  XCTAssertEqualObjects(self.resource.URL, expectedURL);
  XCTAssertEqualObjects(self.resource.name, kMediaName);
  XCTAssertNil(self.resource.licenseServerURL);
}

- (void)testDownload {
  NSString *mediaFile = @"manifestForDownload.mpd";
  [self makeResourceWithMediaFile:mediaFile];
  NSData *fileContents = [self mp4WithName:kRealManifestContentAudio];
  [self generateMocksWithContents:@[ @YES, @YES, @YES ]
                        mediaFile:mediaFile
                  andFileContents:fileContents
              processPsshKeyCalls:3];
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Download completed"];
  [self.resource downloadMediaResourceWithCompletionBlock:^(NSError *error) {
    [expectation fulfill];
  }];
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];
  XCTAssertEqual(self.numTimesDownloadCanceled, 0);
  XCTAssertTrue(self.resource.isDownloaded);
  XCTAssertEqual(self.numTimesDownloadStatusChanged, 1);
  XCTAssertEqual(self.filesDownloaded.count, 4);
  if (self.filesDownloaded.count == 4) {
    NSString *manifestPath = [NSString stringWithFormat:@"/documents/%@/%@", mediaFile, mediaFile];
    XCTAssertEqualObjects(self.filesDownloaded[0].path, manifestPath);
    for (int i = 0; i < 3; i++) {
      XCTAssertEqualObjects(self.filesDownloaded[i + 1],
                            [self generateContentFileURL:i withMediaFile:mediaFile]);
    }
  };
}

- (void)testDelete {
  NSString *mediaFile = @"manifestForDelete.mpd";
  [self makeResourceWithMediaFile:mediaFile];
  NSData *fileContents = [self mp4WithName:kRealManifestContentAudio];
  [self generateMocksWithContents:@[ @YES, @YES, @YES ]
                        mediaFile:mediaFile
                  andFileContents:fileContents
              processPsshKeyCalls:3];
  __weak XCTestExpectation *downloadExpectation =
      [self expectationWithDescription:@"Download completed"];
  [self.resource downloadMediaResourceWithCompletionBlock:^(NSError *error) {
    [downloadExpectation fulfill];
  }];
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];

  // Now that it's downloaded, delete it.
  __weak XCTestExpectation *deleteExpectation = [self expectationWithDescription:@"Deleted"];
  CDMLogInfo(@"Starting delete...");
  [self.resource deleteMediaResourceWithCompletionBlock:^(NSError *error) {
    CDMLogInfo(@"Delete complete!");
    XCTAssertNil(error);
    XCTAssertEqual(self.filesDownloaded.count, 0);
    XCTAssertFalse(self.resource.isDownloaded);
    [deleteExpectation fulfill];
  }];
  CDMLogInfo(@"Start waiting!");
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];
  CDMLogInfo(@"Test complete!");
}

- (void)testDownloadFail {
  NSString *mediaFile = @"manifestForDownloadFail.mpd";
  [self makeResourceWithMediaFile:mediaFile];
  NSData *fileContents = [self mp4WithName:kRealManifestContentAudio];
  [self generateMocksWithContents:@[ @YES, @NO, @YES ]
                        mediaFile:mediaFile
                  andFileContents:fileContents
              processPsshKeyCalls:1];
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Download completed"];
  [self.resource downloadMediaResourceWithCompletionBlock:^(NSError *error) {
    [expectation fulfill];
  }];
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];
  // If a download fails, it should not leave any leftover files in the file system.
  // This is equivalent to the delete action, but it should also prevent further files from being
  // downloaded (thus, the test has the second download failing rather than the third).
  XCTAssertEqual(self.numTimesDownloadCanceled, 1);
  XCTAssertFalse(self.resource.isDownloaded);
  XCTAssertEqual(self.numTimesDownloadStatusChanged, 1);
  XCTAssertEqual(self.filesDownloaded.count, 0);
}

- (void)mediaFile {
  NSString *mediaFile = @"manifestForDownloadFailDueToBadFile.mpd";
  [self makeResourceWithMediaFile:mediaFile];
  NSData *fileContents = [[NSData alloc] init];  // A source file with no PSSH in it.
  [self generateMocksWithContents:@[ @YES, @YES, @YES ]
                        mediaFile:mediaFile
                  andFileContents:fileContents
              processPsshKeyCalls:0];
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Download completed"];
  [self.resource downloadMediaResourceWithCompletionBlock:^(NSError *error) {
    [expectation fulfill];
  }];
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];
  XCTAssertEqual(self.numTimesDownloadCanceled, 1);
  XCTAssertFalse(self.resource.isDownloaded);
  XCTAssertEqual(self.numTimesDownloadStatusChanged, 1);
  XCTAssertEqual(self.filesDownloaded.count, 0);
}

- (void)testFetchLicenseInfo {
  NSString *mediaFile = @"manifestForFetchLicenseInfo.mpd";
  [self makeResourceWithMediaFile:mediaFile];
  [self generateMocksForFetchLicenseInfo];
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"License fetched"];
  [self.resource fetchLicenseInfoWithCompletionBlock:^(NSError *error) {
    [expectation fulfill];
    XCTAssertNil(error);
  }];
  [self waitForExpectationsWithTimeout:kWaitForMediaResourceTimeout handler:nil];
}

// MARK: helpers
- (void)downloadChangedSelector:(NSNotification *)notification {
  self.numTimesDownloadStatusChanged += 1;
}
- (NSURL *)generateContentURL:(int)number {
  return [NSURL URLWithString:[NSString stringWithFormat:@"www.thing.com/file%i.mp4", number]];
}
- (NSURL *)generateContentFileURL:(int)number withMediaFile:(NSString *)mediaFile {
  NSString *urlString = [NSString stringWithFormat:@"/documents/%@/file%i.mp4",
                         mediaFile,
                         number];
  return [NSURL URLWithString:urlString];
}
- (NSData *)mp4WithName:(NSString *)name {
  NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"mp4"];
  return [NSData dataWithContentsOfFile:path];
}
- (void)makeResourceWithMediaFile:(NSString *)mediaFile {
  NSDictionary *json = @{
                         @"media_name" : kMediaName,
                         @"media_thumbnail_url" : kThumbnailURL,
                         @"media_url" : [NSString stringWithFormat:@"www.test.com/%@", mediaFile]
                         };
  self.resource = [[MediaResource alloc] initWithJson:json];
}

// MARK: mocks
- (void)generateMocksForFetchLicenseInfo {
  // Pre-download the files the manifest should contain.
  NSMutableDictionary *contents = [[NSMutableDictionary alloc] init];
  NSString *audioPath =
  [NSString stringWithFormat:@"www.test.com/%@.mp4", kRealManifestContentAudio];
  contents[audioPath] = [self mp4WithName:kRealManifestContentAudio];
  NSString *videoPath =
  [NSString stringWithFormat:@"www.test.com/%@.mp4", kRealManifestContentVideo];
  contents[videoPath] = [self mp4WithName:kRealManifestContentVideo];

  // Make a mock NSData for loading the manifest.
  NSString *path = [[NSBundle mainBundle] pathForResource:kRealManifestName ofType:@"mpd"];
  NSData *fileData = [NSData dataWithContentsOfFile:path];
  id dataMock = [OCMockObject mockForClass:[NSData class]];
  [[[dataMock stub] andReturn:fileData] dataWithContentsOfURL:[OCMArg any]];

  // Make a downloader mock for running partial download.
  void (^partialDownloadBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSURL *downloadURL;
    __unsafe_unretained void (^callback)(NSData *data, NSError *error);
    [invocation getArgument:&downloadURL atIndex:2];
    [invocation getArgument:&callback atIndex:4];
    NSData *data = contents[downloadURL.path];
    XCTAssertNotNil(contents[downloadURL.path]);
    callback(data, nil);
  };
  id downloadMock = [OCMockObject partialMockForObject:[Downloader sharedInstance]];
  id partialDownloadStub = [[[downloadMock stub] ignoringNonObjectArgs] andDo:partialDownloadBlock];
  [partialDownloadStub downloadPartialData:[OCMArg any]
                                 withRange:NSMakeRange(0, 0)
                                completion:[OCMArg any]];

  self.mocks = @[ dataMock, downloadMock ];
}
- (void)generateMocksWithContents:(NSArray *)contents
                        mediaFile:(NSString *)mediaFile
                  andFileContents:(NSData *)fileContents
              processPsshKeyCalls:(int)processPsshKeyCalls {
  // Make a mock for iOSCdm.
  id iOSCdmMock = [OCMockObject mockForClass:[iOSCdm class]];
  [[[iOSCdmMock stub] andReturn:iOSCdmMock] sharedInstance];
  void (^processPsshBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained void (^callback)(NSError *error);
    [invocation getArgument:&callback atIndex:4];
    // TODO (theodab): check the pssh that are sent in to make sure they have the right contents and
    // are sent in the right order
    callback(nil);
  };
  for (int i = 0; i < processPsshKeyCalls; i++) {
    id processPsshKeyExpect = [[[iOSCdmMock expect] ignoringNonObjectArgs] andDo:processPsshBlock];
    [processPsshKeyExpect processPsshKey:[OCMArg any]
                            isOfflineVod:NO
                         completionBlock:[OCMArg any]];
  }

  // Make a mock for NSFileManager to keep track of what is being deleted.
  id mockFM = [OCMockObject mockForClass:[NSFileManager class]];
  id managerMock = [OCMockObject partialMockForObject:[NSFileManager defaultManager]];
  [[[mockFM stub] andReturn:managerMock] defaultManager];
  void (^removeBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSURL *removeURL;
    [invocation getArgument:&removeURL atIndex:2];
    // We only use this to remove directories, so remove everything containing that substring.
    NSMutableArray *newFilesDownloaded = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.filesDownloaded.count; i++) {
      NSURL *downloaded = self.filesDownloaded[i];
      if ([downloaded.path containsString:removeURL.path]) {
        CDMLogInfo(@"Deleted %@", downloaded);
      } else {
        [newFilesDownloaded addObject:downloaded];
      }
    }
    self.filesDownloaded = newFilesDownloaded;
  };
  void (^existsBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSString *filePath;
    [invocation getArgument:&filePath atIndex:2];
    BOOL exists = NO;
    for (NSURL *downloaded in self.filesDownloaded) {
      if ([downloaded.path isEqualToString:filePath]) {
        exists = YES;
        break;
      }
    }
    [invocation setReturnValue:&exists];
  };
  NSURL *documentURL = [NSURL URLWithString:@"/documents/"];
  [[[managerMock stub] andDo:removeBlock] removeItemAtURL:[OCMArg any] error:[OCMArg setTo:nil]];
  [[[managerMock stub] ignoringNonObjectArgs] createDirectoryAtPath:[OCMArg any]
                                        withIntermediateDirectories:YES
                                                         attributes:[OCMArg any]
                                                              error:[OCMArg setTo:nil]];
  id documentURLStub = [[[managerMock stub] ignoringNonObjectArgs] andReturn:@[ documentURL ]];
  [documentURLStub URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
  [[[managerMock stub] andDo:existsBlock] fileExistsAtPath:[OCMArg any]];

  // Make a mock downloader that simulates the given contents.
  id downloadMock = [OCMockObject partialMockForObject:[Downloader sharedInstance]];
  void (^downloadBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSURL *downloadURL;
    __unsafe_unretained NSURL *fileURL;
    [invocation getArgument:&downloadURL atIndex:2];
    [invocation getArgument:&fileURL atIndex:3];

    // pretend to download the manifest, and its contents
    [self.resource downloader:downloadMock
   didStartDownloadingFromURL:downloadURL
                  toFileAtURL:fileURL];
    for (int i = 0; i < contents.count; i++) {
      NSURL *fileURL = [self generateContentFileURL:i withMediaFile:mediaFile];
      [self.resource downloader:downloadMock
     didStartDownloadingFromURL:[self generateContentURL:i]
                    toFileAtURL:fileURL];
    }
    [self.resource downloader:downloadMock
  didFinishDownloadingFromURL:downloadURL
                  toFileAtURL:fileURL];
    [self.filesDownloaded addObject:fileURL];
    for (int i = 0; i < contents.count; i++) {
      NSNumber *succeeded = contents[i];
      NSURL *url = [self generateContentURL:i];
      NSURL *fileURL = [self generateContentFileURL:i withMediaFile:mediaFile];
      if ([succeeded boolValue]) {
        [self.filesDownloaded addObject:fileURL];
        [self.resource downloader:downloadMock
      didFinishDownloadingFromURL:url
                      toFileAtURL:fileURL];
      } else {
        NSError *fakeError = [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:nil];
        [self.resource downloader:downloadMock
         didFailToDownloadFromURL:url
                        withError:fakeError];
        // Now that the download has failed, it should cancel any further downloads.
        break;
      }
    }

    // Return the desired cancel token.
    [invocation setReturnValue:(void *)&kCancelToken];
  };
  void (^partialDownloadBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained void (^callback)(NSData *data, NSError *error);
    [invocation getArgument:&callback atIndex:4];
    // Pass a pre-loaded NSData, just so that it will return something with a PSSH.
    callback(fileContents, nil);
  };
  void (^cancelDownloadBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained CancelToken token;
    [invocation getArgument:&token atIndex:2];
    self.numTimesDownloadCanceled += 1;
    XCTAssertEqualObjects(kCancelToken, token);
  };
  [[[downloadMock stub] andDo:downloadBlock] downloadFromURL:[OCMArg any]
                                                 toFileAtURL:[OCMArg any]
                                                    delegate:[OCMArg any]];
  id partialDownloadStub = [[[downloadMock stub] ignoringNonObjectArgs] andDo:partialDownloadBlock];
  [partialDownloadStub downloadPartialData:[OCMArg any]
                                 withRange:NSMakeRange(0, 0)
                                completion:[OCMArg any]];
  [[[downloadMock stub] andDo:cancelDownloadBlock] cancelDownloadWithCancelToken:[OCMArg any]];

  self.mocks = @[ downloadMock, managerMock, mockFM, iOSCdmMock ];
}

@end
