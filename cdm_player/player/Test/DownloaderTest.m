#import <OCMock/OCMock.h>
#import <XCTest/XCTest.h>

#import "CdmPlayerErrors.h"
#import "Downloader.h"
#import "Logging.h"
#import "MediaResource.h"
#import "Stream.h"

NSInteger const kPartialDownloadLength = 1453;
NSInteger const kPartialDownloadStartLocation = 600;
float const kWaitForAsyncCallbackTimeout = 1.0;
static NSString *const kPartialDownloadName = @"tears_h264_baseline_240p_800";
static NSString *const kDownloadName = @"tears_cenc_small";
static NSString *const kDownloadNameWithExtension = @"tears_cenc_small.mpd";
static NSString *const kDownloadFirstContents = @"tears_audio_eng.mp4";
static NSString *const kDownloadSecondsContents = @"tears_h264_baseline_240p_800.mp4";
static NSString *const kInvalidDownloadName = @"fakefile";
extern NSTimeInterval kDownloadTimeout;
extern NSString *const kRangeHeaderString;

@interface DownloaderDelegateMock : NSObject <DownloaderDelegate>

@property int startDownloadingCallCount;
@property int updateDownloadingCallCount;
@property int failedDownloadingCallCount;
@property int finishedDownloadingCallCount;
@property NSMutableArray *filePaths;
@property NSError *lastError;
@property(nonatomic, copy) void (^startBlock)(int);
@property(nonatomic, copy) void (^finishBlock)(int);

@end

@implementation DownloaderDelegateMock

// Delegate Mock used to track when starting a download.
- (void)downloader:(Downloader *)downloader
    didStartDownloadingFromURL:(NSURL *)sourceURL
                   toFileAtURL:(NSURL *)fileURL {
  self.startDownloadingCallCount += 1;
  if (!self.filePaths) {
    self.filePaths = [[NSMutableArray alloc] init];
  }
  [self.filePaths addObject:fileURL.lastPathComponent];
  if (self.startBlock) {
    self.startBlock(self.startDownloadingCallCount);
  }
}

// Delegate Mock used to track when an update is triggered.
- (void)downloader:(Downloader *)downloader
    didUpdateDownloadProgress:(float)progress
                 forFileAtURL:(NSURL *)fileURL {
  self.updateDownloadingCallCount += 1;
}

// Delegate Mock used to track when a download fails.
- (void)downloader:(Downloader *)downloader
    didFailToDownloadFromURL:(NSURL *)sourceURL
                   withError:(NSError *)error {
  self.lastError = error;
  self.failedDownloadingCallCount += 1;
  if (self.finishBlock) {
    self.finishBlock(self.failedDownloadingCallCount + self.finishedDownloadingCallCount);
  }
}

// Delegate Mock used to track when a download finishes downloading.
- (void)downloader:(Downloader *)downloader
    didFinishDownloadingFromURL:(NSURL *)sourceURL
                    toFileAtURL:(NSURL *)fileURL {
  self.finishedDownloadingCallCount += 1;
  if (self.finishBlock) {
    self.finishBlock(self.failedDownloadingCallCount + self.finishedDownloadingCallCount);
  }
}

@end

@interface DownloaderTest : XCTestCase {
  DownloaderDelegateMock *_delegate;
  DDTTYLogger *_logger;
  int _cancelCallCount;
}
@end

@implementation DownloaderTest

- (void)setUp {
  [super setUp];
  _cancelCallCount = 0;
  _logger = [DDTTYLogger sharedInstance];
  [DDLog addLogger:_logger];
  _delegate = [[DownloaderDelegateMock alloc] init];
}

- (void)tearDown {
  [super tearDown];
  [DDLog removeLogger:_logger];
}

// Force Download failure with a bad URL. Verify the download was started and reports an error.
- (void)testDownloadMPDInvalid {
  Downloader *shared = [Downloader sharedInstance];
  // set up a mock download task
  id mockTask = [self makeMockTask];
  void (^resumeBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    id<NSURLSessionDownloadDelegate> del =
    (id<NSURLSessionDownloadDelegate>)shared.downloadSession.delegate;
    NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:nil];
    [del URLSession:shared.downloadSession task:mockTask didCompleteWithError:error];
  };
  [[[mockTask stub] andDo:resumeBlock] resume];

  // set up the expectation, to wait for the file and it's sub-files to download
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Completion called"];
  _delegate.finishBlock = ^void(int finishCount) {
    if (finishCount == 1) {
      [expectation fulfill];
    }
  };

  [shared downloadFromURL:self.randomURL
              toFileAtURL:self.randomURL
                 delegate:_delegate];

  // wait for the expectation
  [self waitForExpectationsWithTimeout:kDownloadTimeout handler:nil];

  XCTAssertEqual(_delegate.startDownloadingCallCount, 1);
  XCTAssertEqual(_delegate.updateDownloadingCallCount, 0);
  XCTAssertEqual(_delegate.failedDownloadingCallCount, 1);
  XCTAssertEqual(_delegate.finishedDownloadingCallCount, 0);
  XCTAssertEqual(_cancelCallCount, 0);
}

// TODO(theodab): test downloading a manifest with a SegmentTemplate field
// like mp4-live-mpd-AV-BS.mpd, but a smaller one
// to make sure that it can do it successfully, and loads the right contents and such

- (void)testDownloadMPDCancel {
  [self cancelTestInnerWithExpectedNumberOfDownloads:1 andTestsBlock:^{
    // Start a download. Because of the mock, this download will hang while downloading the MPD.
    NSURL *fileURL = [NSURL URLWithString:@"thing.mpd"];
    CancelToken token = [[Downloader sharedInstance] downloadFromURL:self.randomURL
                                                         toFileAtURL:fileURL
                                                            delegate:_delegate];
    XCTAssertNotNil(token);
    // Cancel; this should cancel the simulated MPD download.
    [[Downloader sharedInstance] cancelDownloadWithCancelToken:token];
    XCTAssertEqual(_cancelCallCount, 1);
    XCTAssertEqual(_delegate.finishedDownloadingCallCount, 0);
    // Cancel again; this shouldn't cancel anything further, since there is no remaining download.
    [[Downloader sharedInstance] cancelDownloadWithCancelToken:token];
    XCTAssertEqual(_cancelCallCount, 1);
  }];
}

// TODO: I was getting some test failures on this test earlier, but I submitted it for 30 TAP
// presubmit runs in a row and it didn't fail once so those failures might have just been problems
// with the presubmit system. If there is any errors along the lines of
// '[NSFileManager defaultManager]: unrecognized selector' then it was a real error all along.
- (void)testDownloadMPDContentsCancel {
  [self cancelTestInnerWithExpectedNumberOfDownloads:3 andTestsBlock:^{
    // Download the MPD, but hang on the contents.
    __weak XCTestExpectation *expectationA = [self expectationWithDescription:@"Start called"];
    _delegate.finishBlock = ^void(int finishCount) {
      [expectationA fulfill];
    };
    NSURL *fileURL = [NSURL URLWithString:@"thing.mpd"];
    CancelToken token = [[Downloader sharedInstance] downloadFromURL:self.randomURL
                                                         toFileAtURL:fileURL
                                                            delegate:_delegate];
    XCTAssertNotNil(token);
    // This involves an asynchronous callback, so there's a (very short) gap of time
    [self waitForExpectationsWithTimeout:kWaitForAsyncCallbackTimeout handler:nil];
    // Cancel; this should cancel the two simulated segment downloads.
    [[Downloader sharedInstance] cancelDownloadWithCancelToken:token];
    XCTAssertEqual(_cancelCallCount, 2);
    XCTAssertEqual(_delegate.finishedDownloadingCallCount, 1);
  }];
}

// Downloads a sample manifest with multiple child streams.
// Verifies the delegate methods were triggered and the output file paths match the input file.
- (void)testDownloadMPD {
  NSString *path = [[NSBundle mainBundle] pathForResource:kDownloadName ofType:@"mpd"];
  NSData *fileData = [NSData dataWithContentsOfFile:path];

  // it should start downloading the two content files right after signalling the end of the first
  [self downloadTestInnerWithFileURL:[NSURL URLWithString:kDownloadNameWithExtension]
                         andFileData:fileData];

  // were the delegate methods being called the right number of times?
  XCTAssertEqual(_delegate.startDownloadingCallCount, 3);
  XCTAssertEqual(_delegate.updateDownloadingCallCount, 3);
  XCTAssertEqual(_delegate.failedDownloadingCallCount, 0);
  XCTAssertEqual(_delegate.finishedDownloadingCallCount, 3);
  XCTAssertEqual(_cancelCallCount, 0);

  // check to see the correct files have been loaded
  XCTAssertEqual(_delegate.filePaths.count, 3);
  XCTAssertEqualObjects(kDownloadNameWithExtension, _delegate.filePaths[0]);
  XCTAssertTrue([kDownloadFirstContents isEqualToString:_delegate.filePaths[1]] ||
                [kDownloadFirstContents isEqualToString:_delegate.filePaths[2]]);
  XCTAssertTrue([kDownloadSecondsContents isEqualToString:_delegate.filePaths[1]] ||
                [kDownloadSecondsContents isEqualToString:_delegate.filePaths[2]]);
}

// Verifies downloading the same file twice returns an "Already Downloading" error.
- (void)testAlreadyDownloadingError {
  Downloader *shared = [Downloader sharedInstance];
  NSURL *URL = self.randomURL;
  NSURL *fileURL = self.randomURL;

  // set up a download task mock that simply does nothing
  id mockTask = [self makeMockTask];
  [[mockTask stub] resume];

  // set up a mock for connection that returns the safe template
  id mockConnection = [OCMockObject partialMockForObject:shared.downloadSession];
  [[[mockConnection stub] andReturn:mockTask] downloadTaskWithURL:[OCMArg any]];

  // download the first time; this should start as expected
  __weak XCTestExpectation *startExpectation = [self expectationWithDescription:@"Start called"];
  _delegate.startBlock = ^void(int startCount) {
    if (startCount == 1) {
      [startExpectation fulfill];
    }
  };
  [shared downloadFromURL:URL toFileAtURL:fileURL delegate:_delegate];
  // this involves an asynchronous callback, so there's a (very short) gap of time
  [self waitForExpectationsWithTimeout:kWaitForAsyncCallbackTimeout handler:nil];
  XCTAssertEqual(_delegate.startDownloadingCallCount, 1);

  // download the second time, this should fail to start at all
  __weak XCTestExpectation *failExpectation = [self expectationWithDescription:@"Fail called"];
  _delegate.finishBlock = ^void(int finishCount) {
    if (finishCount == 1) {
      [failExpectation fulfill];
    }
  };
  [shared downloadFromURL:URL toFileAtURL:fileURL delegate:_delegate];
  [self waitForExpectationsWithTimeout:kWaitForAsyncCallbackTimeout handler:nil];
  XCTAssertEqual(_delegate.startDownloadingCallCount, 1);
  XCTAssertEqual(_delegate.failedDownloadingCallCount, 1);
  XCTAssertEqual(_delegate.finishedDownloadingCallCount, 0);
  XCTAssertEqual(_cancelCallCount, 0);
  XCTAssertEqual(_delegate.lastError.code, CdmPlayeriOSErrorCode_AlreadyDownloading);

  // now fail the first request so it's no longer in progress
  NSError *fakeError = [NSError errorWithDomain:NSCocoaErrorDomain code:0 userInfo:nil];
  [shared URLSession:shared.downloadSession
                task:mockTask
didCompleteWithError:fakeError];

  // and run a third request. As the first one is no longer running, this should succeed
  __weak XCTestExpectation *nextStartExpectaion = [self expectationWithDescription:@"Start called"];
  _delegate.startBlock = ^void(int startCount) {
    if (startCount == 2) {
      [nextStartExpectaion fulfill];
    }
  };
  [shared downloadFromURL:URL toFileAtURL:fileURL delegate:_delegate];
  [self waitForExpectationsWithTimeout:kWaitForAsyncCallbackTimeout handler:nil];
  XCTAssertEqual(_delegate.startDownloadingCallCount, 2);

  // Manually cancel all mocks.
  [mockTask stopMocking];
  [mockConnection stopMocking];
}

// Verifies downloading the header of a media file and that the download
// contains all of the expected data.
- (void)testPartialDownload {
  __weak XCTestExpectation *mockExpectation =
      [self expectationWithDescription:@"Patial download mock called"];
  __weak XCTestExpectation *callExpectation =
      [self expectationWithDescription:@"Patial download completion called"];
  NSURL *url = self.randomURL;
  NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:nil];
  NSURLResponse *response = [[NSURLResponse alloc] initWithURL:url
                                                      MIMEType:@"whatever"
                                         expectedContentLength:42
                                              textEncodingName:@"whatever"];
  NSData *data = [NSData data];
  NSRange range = NSMakeRange(0, kPartialDownloadLength);

  void (^block)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSMutableURLRequest *request;
    __unsafe_unretained void (^callback)(NSData *data, NSURLResponse *response, NSError *error);
    [invocation getArgument:&request atIndex:2];
    [invocation getArgument:&callback atIndex:3];
    XCTAssertEqualObjects(request.URL, url);
    callback(data, response, error);
    [mockExpectation fulfill];
  };
  id mock = [self mockDownloadSessionWithCallResult:block];

  Downloader *downloader = [Downloader sharedInstance];
  [downloader downloadPartialData:url
                        withRange:range
                       completion:^(NSData *rData, NSError *rError) {
                         XCTAssertEqualObjects(rData, data);
                         XCTAssertEqualObjects(rError, error);
                         [callExpectation fulfill];
                       }];
  [self waitForExpectationsWithTimeout:kDownloadTimeout handler:nil];

  // Manually cancel all mocks.
  [mock stopMocking];
}

// Verifies downloading a file synchronously will succeed.
- (void)testPartialDownloadBlocking {
  NSURL *URL = self.randomURL;
  NSData *data = [NSData data];
  NSRange range = NSMakeRange(0, kPartialDownloadLength);

  void (^block)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSMutableURLRequest *request;
    __unsafe_unretained void (^callback)(NSData *data, NSURLResponse *response, NSError *error);
    [invocation getArgument:&request atIndex:2];
    [invocation getArgument:&callback atIndex:3];
    XCTAssertEqual(request.URL, URL);
    callback(data, nil, nil);
  };
  id mock = [self mockDownloadSessionWithCallResult:block];

  NSData *rData = [[Downloader sharedInstance] dataDownloadedSynchronouslyFromURL:URL
                                                                        withRange:range];
  XCTAssertEqualObjects(rData, data);

  // Manually cancel all mocks.
  [mock stopMocking];
}

// Verfies setting a range in the HTTP Header will returns the expected amount of data.
- (void)testPartialDownloadWithRange {
  NSRange range = NSMakeRange(kPartialDownloadStartLocation, kPartialDownloadLength);

  void (^block)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSMutableURLRequest *request;
    __unsafe_unretained void (^callback)(NSData *data, NSError *error);
    [invocation getArgument:&request atIndex:2];
    [invocation getArgument:&callback atIndex:3];
    XCTAssertEqual(request.allHTTPHeaderFields.count, 1);
    if (request.allHTTPHeaderFields.count == 1) {
      XCTAssertEqualObjects(request.allHTTPHeaderFields.allKeys[0], kRangeHeaderString);
      int start = kPartialDownloadStartLocation;
      int end = kPartialDownloadStartLocation + kPartialDownloadLength;
      NSString *range = [NSString stringWithFormat:@"bytes=%i-%i", start, end];
      XCTAssertEqualObjects(request.allHTTPHeaderFields.allValues[0], range);
    }
    callback(nil, nil);
  };
  id mock = [self mockDownloadSessionWithCallResult:block];
  [[Downloader sharedInstance] dataDownloadedSynchronouslyFromURL:self.randomURL withRange:range];

  // Manually cancel all mocks.
  [mock stopMocking];
}

#pragma mark private methods

- (void)downloadTestInnerFailure {
  Downloader *shared = [Downloader sharedInstance];

  // set up a mock download task
  id mockTask = [self makeMockTask];
  void (^resumeBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    id<NSURLSessionDownloadDelegate> del =
    (id<NSURLSessionDownloadDelegate>)shared.downloadSession.delegate;
    NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:0 userInfo:nil];
    [del URLSession:shared.downloadSession task:mockTask didCompleteWithError:error];
  };
  [[[mockTask stub] andDo:resumeBlock] resume];

  // set up the expectation, to wait for the file and it's sub-files to download
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Completion called"];
  _delegate.finishBlock = ^void(int finishCount) {
    if (finishCount == 1) {
      [expectation fulfill];
    }
  };

  [shared downloadFromURL:self.randomURL
              toFileAtURL:self.randomURL
                 delegate:_delegate];

  // wait for the expectation
  [self waitForExpectationsWithTimeout:kDownloadTimeout handler:nil];

  // Manually cancel all mocks.
  [mockTask stopMocking];
}

- (void)cancelTestInnerWithExpectedNumberOfDownloads:(int)numDownloads
                                       andTestsBlock:(void (^)())testsBlock {
  Downloader *shared = [Downloader sharedInstance];

  // set up a mock file manager
  id mockFM = [OCMockObject mockForClass:[NSFileManager class]];
  id partialMockFM = [OCMockObject partialMockForObject:[NSFileManager defaultManager]];
  [[[mockFM stub] andReturn:partialMockFM] defaultManager];
  [[partialMockFM stub] moveItemAtURL:[OCMArg any] toURL:[OCMArg any] error:[OCMArg setTo:nil]];

  // set up a mock for data
  NSString *path = [[NSBundle mainBundle] pathForResource:kDownloadName ofType:@"mpd"];
  NSData *fileData = [NSData dataWithContentsOfFile:path];
  id mockData = [OCMockObject mockForClass:[NSData class]];
  [[[mockData stub] andReturn:fileData] dataWithContentsOfURL:[OCMArg any]];

  // set up mocks for each expected download task
  NSMutableArray *mockTasks = [[NSMutableArray alloc] init];
  for (int i = 0; i < numDownloads; i++) {
    id mock = [self makeMockTask];
    void (^resumeBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
      id<NSURLSessionDownloadDelegate> del =
      (id<NSURLSessionDownloadDelegate>)shared.downloadSession.delegate;
      NSURLSession *ses = shared.downloadSession;
      if (i == 0 && numDownloads > 1) {
        // The MPD download of the mock can complete if this mock is expected to get the contents.
        [del URLSession:ses downloadTask:mock didFinishDownloadingToURL:self.randomURL];
      }
    };
    [[[mock stub] andDo:resumeBlock] resume];

    // store the important bits so they don't go out of scope
    [mockTasks addObject:mock];
  }

  // set up a mock for connection
  id mockConnection = [OCMockObject partialMockForObject:shared.downloadSession];
  for (int i = 0; i < numDownloads; i++) {
    // each call to downloadTaskWithURL returns a different download task partial mock
    [[[mockConnection expect] andReturn:mockTasks[i]] downloadTaskWithURL:[OCMArg any]];
  }

  // Run tests.
  testsBlock();

  // Verify the connection mock.
  [mockConnection verify];

  // Manually cancel all mocks.
  [mockFM stopMocking];
  [partialMockFM stopMocking];
  [mockConnection stopMocking];
  [mockData stopMocking];
  for (id mockTask in mockTasks) {
    [mockTask stopMocking];
  }
}

- (void)downloadTestInnerWithFileURL:(NSURL *)fileURL andFileData:(NSData *)fileData {
  Downloader *shared = [Downloader sharedInstance];

  __block NSURL *savedURL;

  // set up a mock file manager
  void (^moveBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained NSURL *from;
    __unsafe_unretained NSURL *to;
    [invocation getArgument:&from atIndex:2];
    [invocation getArgument:&to atIndex:3];
    XCTAssertEqualObjects(from, savedURL);
    savedURL = to;
  };
  id mockFM = [OCMockObject partialMockForObject:[NSFileManager defaultManager]];
  [[[mockFM stub] andReturn:mockFM] defaultManager];
  [[[mockFM stub] andDo:moveBlock] moveItemAtURL:[OCMArg any]
                                           toURL:[OCMArg any]
                                           error:[OCMArg setTo:nil]];

  // set up a mock for data
  id mockData = [OCMockObject mockForClass:[NSData class]];
  [[[mockData stub] andReturn:fileData] dataWithContentsOfURL:[OCMArg any]];

  // set up three mocks, one for each download task
  NSMutableArray *mockTasks = [[NSMutableArray alloc] init];
  for (int i = 0; i < 3; i++) {
    id mock = [self makeMockTask];
    void (^resumeBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
      savedURL = self.randomURL;
      id<NSURLSessionDownloadDelegate> del =
      (id<NSURLSessionDownloadDelegate>)shared.downloadSession.delegate;
      NSURLSession *session = shared.downloadSession;
      [del URLSession:session
                 downloadTask:mock
                 didWriteData:0
            totalBytesWritten:0
    totalBytesExpectedToWrite:1];
      [del URLSession:session downloadTask:mock didFinishDownloadingToURL:savedURL];
    };
    [[[mock stub] andDo:resumeBlock] resume];

    // store the important bits so they don't go out of scope
    [mockTasks addObject:mock];
  }

  // set up a mock for connection
  id mockConnection = [OCMockObject partialMockForObject:shared.downloadSession];
  for (int i = 0; i < 3; i++) {
    // each call to downloadTaskWithURL returns a different download task partial mock
    [[[mockConnection expect] andReturn:mockTasks[i]] downloadTaskWithURL:[OCMArg any]];
  }

  // set up the expectation, to wait for the file and it's sub-files to download
  __weak XCTestExpectation *expectation = [self expectationWithDescription:@"Completion called"];
  _delegate.finishBlock = ^void(int finishCount) {
    if (finishCount == 3) {
      [expectation fulfill];
    }
  };

  [shared downloadFromURL:self.randomURL
              toFileAtURL:fileURL
                 delegate:_delegate];

  // wait for the expectation
  [self waitForExpectationsWithTimeout:kDownloadTimeout handler:nil];
  [mockConnection verify];

  // Manually cancel all mocks.
  [mockFM stopMocking];
  [mockConnection stopMocking];
  [mockData stopMocking];
  for (id mockTask in mockTasks) {
    [mockTask stopMocking];
  }
}

// Mocked Download Session to be used to simulate an active session.
- (id)mockDownloadSessionWithCallResult:(void (^)(NSInvocation *))callResult {
  Downloader *shared = [Downloader sharedInstance];
  id mockDS = [OCMockObject partialMockForObject:shared.downloadSession];

  [[[mockDS stub] andDo:callResult] dataTaskWithRequest:[OCMArg any]
                                      completionHandler:[OCMArg any]];

  return mockDS;
}

- (NSURL *)randomURL {
  // several places in these tests require arbitrary URLs
  // the only important thing is that no two are the same,
  // since the URLs are used internally in hashing
  int randNumber = arc4random_uniform(UINT32_MAX);
  NSString *string = [NSString stringWithFormat:@"FAKEURL#%i", randNumber];
  return [NSURL URLWithString:string];
}

- (id)makeMockTask {
  NSURLRequest *request = [NSURLRequest requestWithURL:self.randomURL];
  id mock = [OCMockObject mockForClass:[NSURLSessionDownloadTask class]];
  [[[mock stub] andReturn:request] originalRequest];
  void (^cancelBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    _cancelCallCount += 1;
  };
  [[[mock stub] andDo:cancelBlock] cancel];
  return mock;
}

@end
