#import <OCMock/OCMock.h>
#import <XCTest/XCTest.h>

#import "MpdParser.h"
#import "Stream.h"
#import "PlaylistBuilder.h"
#import "Logging.h"

static NSString *const kMPDURLString = @"http://www.google.com/path/to.mpd";
static NSString *const kEndList = @"#EXT-X-ENDLIST";
static NSString *const kFileURL = @"tears_h264_baseline_240p_800";

static NSString *const kVideoMPD =
    @"<MPD type=\"static\">"
    @"  <Period>"
    @"    <AdaptationSet id=\"0\" contentType=\"video\" width=\"854\" height=\"480\" "
    @"        frameRate=\"90000/3003\" subsegmentAlignment=\"true\" par=\"16:9\">"
    @"      <Representation id=\"0\" bandwidth=\"2243787\" codecs=\"avc1.42c01f\" "
    @"          mimeType=\"video/mp4\" sar=\"1:1\">"
    @"        <BaseURL>video.mp4</BaseURL>"
    @"        <SegmentBase indexRange=\"804-847\" timescale=\"90000\">"
    @"        <Initialization range=\"0-803\"/>"
    @"        </SegmentBase>"
    @"      </Representation>"
    @"    </AdaptationSet>"
    @"  </Period>"
    @"</MPD>";

@interface StreamTest : XCTestCase
@end

@implementation StreamTest {
  DDTTYLogger *_logger;
  PlaylistBuilder *_playlistBuilder;
}

- (void)setUp {
  [super setUp];
  _logger = [DDTTYLogger sharedInstance];
  [DDLog addLogger:_logger];
  _playlistBuilder = [[PlaylistBuilder alloc] init];
}

- (void)tearDown {
  [super tearDown];
  [DDLog removeLogger:_logger];
}

// Verifies the Stream object was created successfully with real data from a file.
- (void)testStreamInit {
  id cdmMock = [self generateCDMMock];
  Stream *stream = [[Stream alloc] initWithPlaylistBuilder:_playlistBuilder];
  NSString *path = [[NSBundle mainBundle] pathForResource:kFileURL ofType:@"mp4"];
  NSData *initializationData = [[NSData alloc] initWithContentsOfFile:path];
  XCTAssertTrue([stream setUpTransmuxerWithInitializationData:initializationData]);
  OCMVerifyAll(cdmMock);
}

// Verifies the Stream object can be created without an associated PlaylistBuilder object
// and return valid data for a media file.
// Although the scenario creates an unplayable stream, a stream can still be created
// and stored to be used at a later time (offline for example).
- (void)testStreamInitWithNilPlaylistBuilder {
  id cdmMock = [self generateCDMMock];
  _playlistBuilder = nil;
  Stream *stream = [[Stream alloc] initWithPlaylistBuilder:_playlistBuilder];
  NSString *path = [[NSBundle mainBundle] pathForResource:kFileURL ofType:@"mp4"];
  NSData *initializationData = [[NSData alloc] initWithContentsOfFile:path];
  XCTAssertTrue([stream setUpTransmuxerWithInitializationData:initializationData]);
  OCMVerifyAll(cdmMock);
}

// Verifies the Stream has been populated and the output description matches the input.
- (void)testStreamDescription {
  Stream *stream = [[Stream alloc] initWithPlaylistBuilder:_playlistBuilder];
  stream.sourceURL = [[NSURL alloc] initWithString:kMPDURLString];
  NSArray *streamString = [stream.description componentsSeparatedByString:@"="];
  NSString *streamURL = [streamString objectAtIndex:4];
  XCTAssertEqualObjects(streamURL, kMPDURLString);
}

// Verifies the Stream has been created and a usable output HLS file was able to be
// created based on the stored properties.
- (void)testStreamProperties {
  id cdmMock = [self generateCDMMock];
  NSURL *MPDURL = [NSURL URLWithString:kMPDURLString];
  NSString *path = [[NSBundle mainBundle] pathForResource:kFileURL ofType:@"mp4"];
  NSData *initializationData = [[NSData alloc] initWithContentsOfFile:path];

  NSData *MPDData = [kVideoMPD dataUsingEncoding:NSUTF8StringEncoding];
  NSArray *streams = [MPDParser streamArrayFromMPD:_playlistBuilder
                                           MPDData:MPDData
                                           baseURL:MPDURL
                                      storeOffline:NO];
  [streams enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    Stream *stream = obj;
    XCTAssertNotNil(stream);
    XCTAssertTrue([stream setUpTransmuxerWithInitializationData:initializationData]);
    XCTAssertNotNil([NSNumber numberWithUnsignedInteger:stream.DASHIndex->index_count]);
    stream.playlist = [_playlistBuilder childPlaylistForStream:stream];
    NSString *lastLine = [stream.playlist
        substringFromIndex:stream.playlist.length - kEndList.length];
    XCTAssertEqualObjects(lastLine, kEndList);
    OCMVerifyAll(cdmMock);
  }];
}

- (id)generateCDMMock {
  // Make a mock for iOSCdm.
  id iOSCdmMock = [OCMockObject mockForClass:[iOSCdm class]];
  [[[iOSCdmMock stub] andReturn:iOSCdmMock] sharedInstance];
  void (^processPsshBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained void (^callback)(NSError *error);
    [invocation getArgument:&callback atIndex:4];
    // TODO (theodab): check the pssh that are sent in to make sure they have the right contents and
    // are sent in the right order
    callback(nil);
  };
  id processPsshKeyExpect = [[[iOSCdmMock expect] ignoringNonObjectArgs] andDo:processPsshBlock];
  [processPsshKeyExpect processPsshKey:[OCMArg any]
                          isOfflineVod:NO
                       completionBlock:[OCMArg any]];

  return iOSCdmMock;
}

@end
