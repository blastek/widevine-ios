#import <OCMock/OCMock.h>
#import <XCTest/XCTest.h>

#import "Downloader.h"
#import "Stream.h"
#import "Logging.h"
#import "PlaylistBuilder.h"

static NSString *const kManifestURL_eDash = @"tears_cenc_small";
static NSString *const kManifestURL_Clear = @"tears_clear_small";
static NSString *const kManifestURL_templateTimeline = @"tears_template_timeline";
static NSString *const kHLSEndList = @"#EXT-X-ENDLIST";


static NSInteger const kExpectedStreams = 2;

extern float kPartialDownloadTimeout;

@interface PlaylistBuilderTest : XCTestCase {
  DDTTYLogger *_logger;
  PlaylistBuilder *_playlistBuilder;
}
@end

@implementation PlaylistBuilderTest

- (void)setUp {
  _logger = [DDTTYLogger sharedInstance];
  [DDLog addLogger:_logger];
  _playlistBuilder = [[PlaylistBuilder alloc] init];
}

- (void)tearDown {
  [DDLog removeLogger:_logger];
}

- (void)testManifestURL_Clear {
  id cdmMock = [self generateCDMMockWithProcessPsshKeyCalls:0];
  [self convertMPDtoHLS:kManifestURL_Clear expectedStreams:kExpectedStreams downloadData:YES];

  // check if the expected calls have happened
  OCMVerifyAll(cdmMock);
}

- (void)testManifestURL_eDash {
  id cdmMock = [self generateCDMMockWithProcessPsshKeyCalls:4];
  [self convertMPDtoHLS:kManifestURL_eDash expectedStreams:kExpectedStreams downloadData:YES];

  // check if the expected calls have happened
  OCMVerifyAll(cdmMock);
}

- (void)testSegmentTemplateTimeline {
  [self convertMPDtoHLS:kManifestURL_templateTimeline
        expectedStreams:kExpectedStreams
         downloadData:NO];
  XCTAssertEqual(_playlistBuilder.streams.count, kExpectedStreams);
  for (Stream *stream in _playlistBuilder.streams) {
    NSInteger length = [[stream.playlist componentsSeparatedByCharactersInSet:
                         [NSCharacterSet newlineCharacterSet]] count];
    // Check length and properties of Stream and HLS playlist.
    if (stream.isVideo) {
      XCTAssertTrue([stream.playlist containsString:kHLSEndList]);
      XCTAssertEqual(length, 374); // Expected HLS Playlist Length
      XCTAssertEqual(stream.bandwidth, 405000);
      XCTAssertEqualObjects(stream.codecs, @"avc1.42C014");
    } else {
      XCTAssertTrue([stream.playlist containsString:kHLSEndList]);
      XCTAssertEqual(length, 742); // Expected HLS Playlist Length
      XCTAssertEqual(stream.bandwidth, 127997);
      XCTAssertEqualObjects(stream.codecs, @"mp4a.40.2");
    }
  }
}

#pragma mark private methods

// Creates an output of an HLS Playlist from a MPD.
- (void)convertMPDtoHLS:(NSString *)mpdURL
        expectedStreams:(int)expectedStreams
        downloadData:(BOOL)downloadData {
  __weak XCTestExpectation *streamExpectation =
      [self expectationWithDescription:@"Stream completion called"];

  // use the custom callback version of processMpd, so that the downloader doesn't get involved
  _playlistBuilder.MPDURL = [[NSBundle mainBundle] URLForResource:mpdURL withExtension:@"mpd"];
  [_playlistBuilder processMPD:_playlistBuilder.MPDURL
                  downloadData:downloadData
                withCompletion:^(NSError *error) {
    [streamExpectation fulfill];
    XCTAssertNil(error, @"MPD failed to load with error %@", error);
  }];

  // wait for the MPD to finish loading
  [self waitForExpectationsWithTimeout:0.5 handler:nil];  // hopefully 0.5s is long enough

  if (downloadData) {
    // because expectations are blocking, I can just put the rest down here
    if (_playlistBuilder.streams) {
      NSArray *streams = _playlistBuilder.streams;
      XCTAssertEqual(streams.count, expectedStreams);
      if (streams.count == expectedStreams) {
        [self downloadStreams:streams];
      }
    }
  }
}

// downloads the streams, and test to see if the contents are as expected
- (void)downloadStreams:(NSArray<Stream *> *)streams {
  for (Stream *stream in streams) {
    CDMLogInfo(@"Stream %@", stream);

    NSData *data = [NSData dataWithContentsOfURL:stream.sourceURL];

    XCTAssertNotNil(data);

    if (data) {
      BOOL initialized = [stream setUpTransmuxerWithInitializationData:data];
      XCTAssertTrue(initialized);

      if (initialized) {
        NSString *m3u8 = [_playlistBuilder childPlaylistForStream:stream];
        NSString *endList = [m3u8 substringFromIndex:[m3u8 length] - 14];
        // Verify the End of the list is present and completed.
        XCTAssertEqualObjects(endList, @"#EXT-X-ENDLIST", @"Bad HLS Playlist");
      }
    }
  }
}

- (id)generateCDMMockWithProcessPsshKeyCalls:(int)processPsshKeyCalls {
  // Make a mock for iOSCdm.
  id iOSCdmMock = [OCMockObject mockForClass:[iOSCdm class]];
  [[[iOSCdmMock stub] andReturn:iOSCdmMock] sharedInstance];
  void (^processPsshBlock)(NSInvocation *) = ^(NSInvocation *invocation) {
    __unsafe_unretained void (^callback)(NSError *error);
    [invocation getArgument:&callback atIndex:4];
    // TODO (theodab): check the pssh that are sent in to make sure they have the right contents and
    // are sent in the right order
    callback(nil);
  };
  for (int i = 0; i < processPsshKeyCalls; i++) {
    id processPsshKeyExpect = [[[iOSCdmMock expect] ignoringNonObjectArgs] andDo:processPsshBlock];
    [processPsshKeyExpect processPsshKey:[OCMArg any]
                            isOfflineVod:NO
                         completionBlock:[OCMArg any]];
  }

  return iOSCdmMock;
}

@end
