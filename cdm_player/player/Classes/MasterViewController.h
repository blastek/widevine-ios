// Copyright 2015 Google Inc. All rights reserved.

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController
@property(nonatomic, strong) DetailViewController *detailViewController;
@end
