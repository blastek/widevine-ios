// Copyright 2015 Google Inc. All rights reserved.

#import "Downloader.h"
#import "MasterViewController.h"

@class MasterViewController;

typedef void (^MediaResourceCompletionBlock)(NSError *error);
extern NSString * const kDownloadedStatusChangedNotification;

@interface MediaResource : NSObject <DownloaderDelegate>

@property(nonatomic) NSMutableDictionary<NSURL *, NSNumber *> *downloads;
@property(nonatomic, strong) NSMutableArray *filesBeingDownloaded;
// Query CDM for License Status and Expiration.
@property(nonatomic) BOOL getLicenseInfo;
@property(nonatomic, assign) int percentage;
@property(nonatomic, strong) NSURL *licenseServerURL;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSData *PSSH;
// Offline License File ready to be deleted.
@property BOOL releaseLicense;
@property(nonatomic, strong) NSURL *thumbnailURL;
@property(nonatomic, strong) UIImage *thumbnailImage;
@property(nonatomic, assign) int thumbnailLoadTries;
@property(nonatomic, strong) NSURL *URL;
@property(nonatomic) NSRange initRange;

- (instancetype)initWithJson:(NSDictionary *)jsonDictionary;

// Download the media resource's contents and then call the completion block on the main queue.
// This method is meant to be called on the main queue.
- (void)downloadMediaResourceWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock;
// Delete the media resource's contents and then call the completion block on the main queue.
// This method is meant to be called on the main queue.
- (void)deleteMediaResourceWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock;
// Print out the license.
// This method is meant to be called on the main queue.
- (void)fetchLicenseInfoWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock;
// Returns true if the associated files are downloaded.
- (BOOL)isDownloaded;
// Returns the offlinePath of the resource.
- (NSURL *)offlinePath;

@end
