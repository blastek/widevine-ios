// Copyright 2015 Google Inc. All rights reserved.

#import "CdmWrapper.h"
#import "LiveStream.h"
#import "UDTApi.h"

struct DashToHlsIndex;
struct DashToHlsSession;
@class PlaylistBuilder;

// Object that contains an individual stream within a playlist before that stream is transmuxed
// to DASH (Dynamic Adaptive Streaming over HTTP) content via the UDT (DASH Transmuxer).
// Each property below is parsed from an incoming playlist, typically an
// MPD (Media Presentation Decription) and stored in the stream object.
// Every stream (separate bitrate, audio, video, etc) contains
// it own set of properties that will need to be passed to the Transmuxer.
// DASH Transmuxer: https://github.com/google/universal-dash-transmuxer
// DASH-MPEG:
//     http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=57623
// HLS: https://developer.apple.com/streaming/

// Initialized via the PlaylistBuilder object and represents a single incoming stream before
// playback.
// The properties of the stream will be used to create output media that is playbable on iOS.
@interface Stream : NSObject

// Input DASH Playlist type necessary for determining
// how to create HLS (Apple HTTP Live Streaming) Playlist (m3u8).
// See DASH-MPEG link above for details of each media type.
typedef NS_ENUM(NSInteger, DASHMediaType) {
  DASHMediaTypeUnknown,
  DASHMediaTypeSegmentBase,
  DASHMediaTypeSegmentListDuration,
  DASHMediaTypeSegmentListTimeline,
  DASHMediaTypeSegmentTemplateDuration,
  DASHMediaTypeSegmentTemplateTimeline,
};

#pragma mark - Stream Methods

// Init method with corresponding PlaylistBuilder object.
- (id)initWithPlaylistBuilder:(PlaylistBuilder *)playlistBuilder;

// Initiates the Transmuxing of content by passing DASH data.
- (BOOL)setUpTransmuxerWithInitializationData:(NSData *)initializationData;

#pragma mark - DASH Transmuxer (UDT) Setup Methods

// Associates the stream with a handler that communicates the status
// when decrypting (asynchronously) the media contents via the DASH Transmuxer (UDT).
- (DashToHlsStatus)setDecryptionHandler:(DashToHlsContext)handler;

// Associates the stream with a handler that will be used to asynchronously read
// the PSSH/Content Key from the media that will be used to create a license request.
- (DashToHlsStatus)setPSSHHandler:(DashToHlsContext)handler;

// Parses the first segment (or Initialization segment) to extract all relevant data in the media
// file that will be used during Transmuxing.
- (DashToHlsStatus)parseInitializationData:(NSData *)initializationData;

#pragma mark - Basic Media File Properties

// URL of the physical media file.
@property NSURL *sourceURL;
// Bandwidth (in bps).
@property NSInteger bandwidth;
// Codec string used to filter out supported and non-supported codecs before transmuxing.
@property NSString *codecs;
// Video height.
@property NSInteger height;
// Video Width.
@property NSInteger width;
// MIME Type of the stream (typically, but not limited to, video/mp4 or audio/mp4).
@property NSString *mimeType;
// Whether this is a video stream.
@property(getter=isVideo) BOOL video;

#pragma mark - DASH specific stream Proprerties

// Stores the type of DASH template used (SegmentBase, SegmentTemplate, etc).
@property DASHMediaType DASHMediaType;
// Stores the completion status of the stream after it has been processed and is ready to play.
@property(getter=isProcessingDone) BOOL processingDone;
// Maintains the index (or count) of DASH segments that will be used to
// determine how many TS segments will need to be created.
@property struct DashToHlsIndex *DASHIndex;
// Contains the byte range to be used for transmuxing.
@property NSRange initialRange;
// Indicates whether the stream is a live broadcast stream or on-demand stream.
@property(getter=isLive) BOOL live;
// PlaylistBuilder object that contains the Stream object.
@property LiveStream *liveStream;
// The output playlist in data form (m3u8 format) used when creating the output M3U8.
@property NSString *playlist;
// Value of PSSH to be passed into UDT (DASH Transmuxer).
@property NSData *PSSH;
// Full duration of the clip stored in seconds after being converted from ISO 8601 format.
// https://en.wikipedia.org/wiki/ISO_8601#Durations
// Optional, may not be present in DASH manifest.
@property NSInteger mediaPresentationDuration;
// Session to be used when Transmuxing with UDT (DASH Transmuxer).
@property struct DashToHlsSession *session;
// Associated PlaylistBuilder object used to create a playable stream based on the Stream
// properties.
// TODO(seawardt@): Move PlaylistBuilder out, and address streamReady from PlaylistBuilder
// object directly.
@property(nonatomic, weak) PlaylistBuilder *playlistBuilder;
// Index of this single stream in relation to all streams.
@property NSInteger streamIndex;

@end
