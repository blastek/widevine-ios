// Copyright 2015 Google Inc. All rights reserved.

#import "Downloader.h"

// TODO(seawardt@): Move project to third_party.
#import "CdmPlayerErrors.h"
#import "CdmPlayerHelpers.h"
#import "Logging.h"
#import "MpdParser.h"

// Contains information for each download.
@interface DownloadInfo : NSObject
@property(nonatomic) CancelToken cancelToken;
@property(nonatomic, weak, readonly) id<DownloaderDelegate> delegate;
@property(nonatomic, readonly) NSURL *fileURL;
@property(nonatomic, readonly) NSURLSessionDownloadTask *task;
@end

@implementation DownloadInfo
// Sets the download info for the given task.
- (instancetype)initWithFileURL:fileURL
                           task:(NSURLSessionDownloadTask *)task
                    cancelToken:(CancelToken)cancelToken
                       delegate:(id<DownloaderDelegate>)delegate {
  _delegate = delegate;
  _fileURL = fileURL;
  _task = task;
  _cancelToken = cancelToken;
  return self;
}

@end

static NSString *const kMPDString = @"mpd";
NSString *const kRangeHeaderString = @"Range";
NSTimeInterval const kDownloadTimeout = 10.0;

@interface Downloader ()
// Maps URLs to objects containing information about the corresponding downloads.
@property(nonatomic) NSMutableDictionary<NSURL *, DownloadInfo *> *downloadInfoForURL;
// Handles each callback for a download. Set to the main queue.
@property dispatch_queue_t delegateQueue;
@end

@implementation Downloader

// TODO (theodab): Sometimes the downloader causes an EXEC_BAD_ACCESS.
// This seems to be a simulator-only NSURLSession bug.
// See http://stackoverflow.com/questions/40371536/nsurlsession-causing-exc-bad-access

- (instancetype)init {
  if (self = [super init]) {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.timeoutIntervalForRequest = kDownloadTimeout;
    config.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    _downloadSession = [NSURLSession sessionWithConfiguration:config
                                                     delegate:self
                                                delegateQueue:nil];
    _downloadInfoForURL = [[NSMutableDictionary<NSURL *, DownloadInfo *> alloc] init];
    _delegateQueue = dispatch_get_main_queue();
  }
  return self;
}

+ (instancetype)sharedInstance {
  static Downloader *downloader = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    downloader = [[Downloader alloc] init];
  });
  return downloader;
}

- (CancelToken)downloadFromURL:(NSURL *)URL
                   toFileAtURL:(NSURL *)fileURL
                      delegate:(id<DownloaderDelegate>)delegate {
  CancelToken token = URL.path;
  [self downloadFromURL:URL toFileAtURL:fileURL delegate:delegate withCancelToken:token];
  return token;
}

- (void)downloadFromURL:(NSURL *)URL
            toFileAtURL:(NSURL *)fileURL
               delegate:(id<DownloaderDelegate>)delegate
        withCancelToken:(CancelToken)token {
  // make the download task
  NSURLSessionDownloadTask *task = [self.downloadSession downloadTaskWithURL:URL];
  if ([self downloadInfoForTask:task] != nil) {
    // someone is already downloading something with the same URL!
    // TODO(seawardt@): Change to cdm_cdmErrorWithCode:userInfo: as it is a category method.
    NSError *error = [NSError cdmErrorWithCode:CdmPlayeriOSErrorCode_AlreadyDownloading
                                      userInfo:nil];
    dispatch_async(self.delegateQueue, ^{
      [delegate downloader:self didFailToDownloadFromURL:URL withError:error];
    });
    return;
  }

  // save all of the session-related data into the session info dict, for safekeeping
  DownloadInfo *info = [[DownloadInfo alloc] initWithFileURL:fileURL
                                                        task:task
                                                 cancelToken:token
                                                    delegate:delegate];
  [self setDownloadInfo:info forTask:task];

  // and now start the download task
  CDMLogInfo(@"Downloading data at %@.", URL);
  dispatch_async(self.delegateQueue, ^{
    [delegate downloader:self didStartDownloadingFromURL:URL toFileAtURL:fileURL];
  });

  [task resume];
}

// Creates a string to be used when requesting a range of data from a URL based on NSRange.
- (NSString *)rangeHeaderString:(NSRange)range {
  return [NSString stringWithFormat:@"bytes=%lu-%lu",
                                    (unsigned long)range.location,
                                    (unsigned long)NSMaxRange(range)];
}

- (void)cancelDownloadWithCancelToken:(CancelToken)token {
  @synchronized(self) {
    for (DownloadInfo *info in self.downloadInfoForURL.allValues) {
      if ([info.cancelToken isEqualToString:token]) {
        // That downloader was started by that delegate.
        [info.task cancel];
        [self deleteDownloadInfoForTask:info.task];
      }
    }
  }
}


- (void)downloadPartialData:(NSURL *)URL
                  withRange:(NSRange)range
                 completion:(void (^)(NSData *data, NSError *error)) completion {
  CDMLogInfo(@"Downloading data from %@.", URL);

  if ([URL isFileURL]) {
    // it's a local file
    NSError *error;
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingFromURL:URL error:&error];
    if (error) {
      completion(nil, error);
      return;
    }
    [fileHandle seekToFileOffset:range.location];
    NSData *data = [fileHandle readDataOfLength:range.length];
    completion(data, nil);
    return;
  }

  // format a request to download from that URL, optionally with a byte range
  NSURLRequestCachePolicy policy = NSURLRequestReloadIgnoringLocalCacheData;
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL
                                                         cachePolicy:policy
                                                     timeoutInterval:kDownloadTimeout];

  if (range.length != 0) {
    [request setValue:[self rangeHeaderString:range] forHTTPHeaderField:kRangeHeaderString];
  }

  void (^wrappedCompletionHandler)(NSData *, NSURLResponse *, NSError *) = NULL;
  wrappedCompletionHandler = ^(NSData * _Nullable data,
                               NSURLResponse * _Nullable response,
                               NSError * _Nullable error) {
    completion(data, error);
  };
  NSURLSessionDataTask *task = [self.downloadSession dataTaskWithRequest:request
                                                       completionHandler:wrappedCompletionHandler];
  [task resume];
}

- (NSData *)dataDownloadedSynchronouslyFromURL:(NSURL *)URL withRange:(NSRange)range {
  dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
  __block NSData *downloadedData = nil;
  void (^completion)(NSData *, NSError *);
  completion = ^(NSData * _Nullable data, NSError * _Nullable error) {
    downloadedData = data;
    dispatch_semaphore_signal(semaphore);
  };

  [self downloadPartialData:URL withRange:range completion:completion];
  dispatch_time_t timeOut = dispatch_time(DISPATCH_TIME_NOW,
                                          (uint64_t)(NSEC_PER_SEC * kDownloadTimeout));
  dispatch_semaphore_wait(semaphore, timeOut);
  return downloadedData;
}

#pragma mark - helpers

// Dispatches an error to the delegate for the provided download info.
- (void)dispatchError:(NSError *)error forDownload:(DownloadInfo *)info withSourceURL:(NSURL *)URL {
  dispatch_async(self.delegateQueue, ^{
    [info.delegate downloader:self didFailToDownloadFromURL:URL withError:error];
  });
}

// Accesses the download info for the given task.
- (DownloadInfo *)downloadInfoForTask:(NSURLSessionTask *)task {
  @synchronized (self) {
    return self.downloadInfoForURL[task.originalRequest.URL];
  }
}

// Sets the download info for the given task.
- (void)setDownloadInfo:(DownloadInfo *)info forTask:(NSURLSessionTask *)task {
  @synchronized (self) {
    self.downloadInfoForURL[task.originalRequest.URL] = info;
  }
}

// Removes the DownloadInfo object from the dictionary that maintains ongoing downloads.
- (void)deleteDownloadInfoForTask:(NSURLSessionTask *)task {
  @synchronized (self) {
    [self.downloadInfoForURL removeObjectForKey:task.originalRequest.URL];
  }
}

#pragma mark - NSURLSessionDownloaderDelegate

- (void)URLSession:(NSURLSession *)session
                    task:(NSURLSessionTask *)task
    didCompleteWithError:(NSError *)error {
  if (error) {
    DownloadInfo *info = [self downloadInfoForTask:task];
    [self deleteDownloadInfoForTask:task];
    [self dispatchError:error forDownload:info withSourceURL:task.originalRequest.URL];
  }
}

- (void)URLSession:(NSURLSession *)session
                 downloadTask:(NSURLSessionDownloadTask *)downloadTask
    didFinishDownloadingToURL:(NSURL *)location {
  // move the file from its temporary location to a permanent one
  NSFileManager *fileManager = [NSFileManager defaultManager];

  DownloadInfo *info = [self downloadInfoForTask:downloadTask];
  NSURL *URL = downloadTask.originalRequest.URL;
  NSURL *fileURL = info.fileURL;
  CDMLogInfo(@"Moving file from temporary location %@ to permanent location %@.",
             location,
             fileURL);

  NSError *error;
  [fileManager moveItemAtURL:location toURL:fileURL error:&error];
  if (error) {
    [self dispatchError:error forDownload:info withSourceURL:downloadTask.originalRequest.URL];
    return;
  }
  [self deleteDownloadInfoForTask:downloadTask];

  if ([fileURL.pathExtension isEqualToString:kMPDString]) {
    // download the files referenced by the MPD
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    // TODO (theodab): properly parse manifests with SegmentTemplate fields
    // since we have one and it has a download button
    NSArray<Stream *> *parsed = [MPDParser streamArrayFromMPD:nil
                                                      MPDData:data
                                                      baseURL:URL
                                                 storeOffline:YES];
    if (parsed.count == 0) {
      // delete the manifest
      [fileManager removeItemAtURL:fileURL error:nil];
      error = [NSError cdmErrorWithCode:CdmPlayeriOSErrorCode_EmptyMPD userInfo:nil];
      [self dispatchError:error forDownload:info withSourceURL:downloadTask.originalRequest.URL];
      return;
    }
    for (Stream *stream in parsed) {
      NSURL *streamURL = CDMDocumentFileURLForFilename(stream.sourceURL.lastPathComponent,
                                                       fileURL.lastPathComponent);
      [self downloadFromURL:stream.sourceURL
                toFileAtURL:streamURL
                   delegate:info.delegate
            withCancelToken:info.cancelToken];
    }
  }

  dispatch_async(self.delegateQueue, ^{
    [info.delegate downloader:self
        didFinishDownloadingFromURL:URL
                        toFileAtURL:fileURL];
  });
}

- (void)URLSession:(NSURLSession *)session
             downloadTask:(NSURLSessionDownloadTask *)downloadTask
             didWriteData:(int64_t)bytesWritten
        totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
  DownloadInfo *info = [self downloadInfoForTask:downloadTask];
  float progress;
  if (totalBytesExpectedToWrite == 0) {
    progress = 0;
  } else {
    progress = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
  }
  dispatch_async(self.delegateQueue, ^{
    [info.delegate downloader:self didUpdateDownloadProgress:progress forFileAtURL:info.fileURL];
  });
}

@end
