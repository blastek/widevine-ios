// Copyright 2015 Google Inc. All rights reserved.
#import <AVFoundation/AVFoundation.h>
#import "PlaybackView.h"

#import "PlayerControlsView.h"
#import "PlayerScrubberView.h"

@implementation PlaybackView

const float kAspectRatio = 0.5625;
const int kIconSize = 24;
const int kElementHeight = 44;
const int kElementWidth = 70;

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    self.backgroundColor = [UIColor blackColor];

    _controlsView = [[PlayerControlsView alloc] init];
    _controlsView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_controlsView];

    _scrubberView = [[PlayerScrubberView alloc] init];
    _scrubberView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_scrubberView];

    // Layout views.
    NSDictionary *visualFormatDict = @{@"S": _scrubberView, @"C": _controlsView};
    NSDictionary *metrics = @{@"H": [NSNumber numberWithInt:kElementHeight]};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[S]-0-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:visualFormatDict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[C]-0-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:visualFormatDict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[S(==H)]"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:visualFormatDict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[C(==H)]-0-|"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:visualFormatDict]];
  }
  return self;
}

- (void)setFullscreen:(BOOL)fullscreen {
  _fullscreen = fullscreen;
  [self setNeedsLayout];
}

- (void)layoutSubviews {
  [self bringSubviewToFront:_scrubberView];
  [self bringSubviewToFront:_controlsView];
}

+ (Class)layerClass {
  return [AVPlayerLayer class];
}

- (AVPlayer *)player {
  return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
  [(AVPlayerLayer *)[self layer] setPlayer:player];
}

/* Specifies how the video is displayed within a player layer’s bounds.
 (AVLayerVideoGravityResizeAspect is default) */
- (void)setVideoFillMode:(NSString *)fillMode {
  AVPlayerLayer *playerLayer = (AVPlayerLayer *)[self layer];
  playerLayer.videoGravity = fillMode;
}

@end
