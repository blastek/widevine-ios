// Copyright 2017 Google Inc. All rights reserved.
#import <Foundation/Foundation.h>

@class Downloader;

typedef NSString *CancelToken;

// Delegate to communicate the status of a download.
@protocol DownloaderDelegate <NSObject>

// Signals that the downloader has started pulling a file and saving it to disk.
- (void)downloader:(Downloader *)downloader
    didStartDownloadingFromURL:(NSURL *)sourceURL
                   toFileAtURL:(NSURL *)fileURL;

// Checks the progress of the ongoing download and reports back a percentage between 0.0 and 1.0.
- (void)downloader:(Downloader *)downloader
    didUpdateDownloadProgress:(float)progress
                 forFileAtURL:(NSURL *)fileURL;

// Signals that the download is complete.
- (void)downloader:(Downloader *)downloader
    didFinishDownloadingFromURL:(NSURL *)sourceURL
                    toFileAtURL:(NSURL *)fileURL;

// Signals that the download has failed, with the attached error.
- (void)downloader:(Downloader *)downloader
    didFailToDownloadFromURL:(NSURL *)sourceURL
                   withError:(NSError *)error;
@end

// TODO(seawardt@): Move NSURLSessionDownloadDelegate annotation to internal class interface.
// Download class to start a remote download and update the app based on progress.
// Downloads occur asynchronously on the main queue.
@interface Downloader : NSObject <NSURLSessionDownloadDelegate>

+ (instancetype)sharedInstance;

// Initializes the downloader.
// All of the delegate methods are called asynchronously on the main queue.
- (instancetype)init;

// Downloads a remote URL and store locally on device.
// If |downloadURL| points to a manifest file, additional downloads will be spawned using different
// URLs than the original request.
// The delegate is responsible for handling each of the separate callbacks, which will be
// delivered on the main queue.
- (CancelToken)downloadFromURL:(NSURL *)URL
                   toFileAtURL:(NSURL *)fileURL
                      delegate:(id<DownloaderDelegate>)delegate;

// Cancels the download associated with the given CancelToken, and any downloads it started.
- (void)cancelDownloadWithCancelToken:(CancelToken)token;

// Pulls data from a media file URL based on the byte-range supplied.
// The URL may be local (file://) or remote (http://).
// |range| may encompass the entire file or only a portion of the file.
// Data is downloaded synchronously to ensure the requested data has been completly retrieved
// before moving on.
- (void)downloadPartialData:(NSURL *)URL
                  withRange:(NSRange)range
                 completion:(void (^)(NSData *data, NSError *error)) completion;

// Downloads data synchronously that will be read or converted. Data is not saved to disk.
- (NSData *)dataDownloadedSynchronouslyFromURL:(NSURL *)URL withRange:(NSRange)range;

// TODO(seawardt@): Add Class extension (Downloader+Test).
// Downloader URL sessions. Exposed to allow mocking in unit tests.
@property(strong, nonatomic) NSURLSession *downloadSession;

@end
