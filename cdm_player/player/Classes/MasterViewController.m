// Copyright 2015 Google Inc. All rights reserved.

#import "MasterViewController.h"

#import "AppDelegate.h"
#import "CdmPlayerErrors.h"
#import "CdmPlayerHelpers.h"
#import "DetailViewController.h"
#import "Logging.h"
#import "MediaCell.h"
#import "MediaResource.h"
#import "Reachability.h"
#import "RecordingDetector.h"

static NSString *kAlertButtonTitle = @"OK";
static NSString *kAlertErrorConnection = @"No network connection available.";
static NSString *kAlertErrorTitle = @"Error";
static NSString *kAlertErrorWifi = @"Must be on WiFi to download.";
static NSString *kAlertErrorRecording = @"Please do not record this app, or use screen mirroring.";
static NSString *kAlertTitle = @"Info";
static NSString *kPlaylistTitle = @"Playlist";

int const kMaxThumbnailLoadTries = 5;

@interface MasterViewController ()<MediaCellDelegate>
@property(nonatomic, strong) NSMutableArray *mediaResources;
@property(nonatomic, strong) NSIndexPath *offlineIndexPath;
@property(nonatomic, strong) NSIndexPath *selectedIndexPath;
@property(nonatomic, strong) NSURLSession *thumbnailSession;
@property(nonatomic, strong) Reachability *reachability;
@property(nonatomic) NetworkStatus lastNetworkStatus;
@end

@implementation MasterViewController

- (instancetype)init {
  self = [super initWithStyle:UITableViewStylePlain];
  if (self) {
    self.title = kPlaylistTitle;
    _mediaResources = [NSMutableArray array];
    _detailViewController = (DetailViewController *)[
        [self.splitViewController.viewControllers lastObject] topViewController];
    // Load Media from JSON file (mediaResources.json)
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"mediaResources" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    _thumbnailSession = [NSURLSession sessionWithConfiguration:config];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData
                                                         options:NSJSONReadingAllowFragments
                                                           error:&error];
    if (error == nil) {
      for (NSDictionary *jsonDictionary in jsonArray) {
        MediaResource *mediaResource = [[MediaResource alloc] initWithJson:jsonDictionary];
        if (mediaResource) {
          [_mediaResources addObject:mediaResource];
        }
      }
    }
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [[NSNotificationCenter defaultCenter] addObserver:self.tableView
                                           selector:@selector(reloadData)
                                               name:kDownloadedStatusChangedNotification
                                             object:nil];
  self.lastNetworkStatus = ReachableViaWiFi;
}

- (void)startReachabilityTracking {
  if (!self.reachability) {
    // subscribe to reachability notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged)
                                                 name:kReachabilityChangedNotification
                                               object:nil];

    self.reachability = [Reachability reachabilityForInternetConnection];
    dispatch_async(dispatch_get_main_queue(), ^{
      // Apple's reachability manager code always runs on the current run loop, so the notification
      // start must be called in the main queue.
      [self.reachability startNotifier];
    });
    // reload the table in case there is in fact no network access
    self.lastNetworkStatus = self.reachability.currentReachabilityStatus;
    [self.tableView reloadData];
  }
}

#pragma mark - Reachability

- (void)reachabilityChanged {
  // reload the table view, so download buttons will appear/disappear and label colors will change
  self.lastNetworkStatus = self.reachability.currentReachabilityStatus;
  [self.tableView reloadData];
}

#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  // This suppresses a warning about table view cell height.
  return 44;
}

- (CGFloat)tableView:(UITableView *)tableView
estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
  // This suppresses a warning about table view cell height.
  return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return _mediaResources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *const sReuseIdentifier = @"mediaCell";
  MediaResource *mediaResource = _mediaResources[indexPath.row];

  MediaCell *mediaCell = [tableView dequeueReusableCellWithIdentifier:sReuseIdentifier];
  if (!mediaCell) {
    mediaCell = [[MediaCell alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:sReuseIdentifier];
  }
  mediaCell.textLabel.text = mediaResource.name;
  mediaCell.canStream = self.lastNetworkStatus != NotReachable;
  mediaCell.canDownload = self.lastNetworkStatus == ReachableViaWiFi;
  mediaCell.isDownloaded = mediaResource.isDownloaded;
  mediaCell.filesBeingDownloaded = mediaResource.filesBeingDownloaded;
  mediaCell.percentage = mediaResource.percentage;
  if (mediaResource.thumbnailImage) {
    mediaCell.thumbnail = mediaResource.thumbnailImage;
  } else if (self.lastNetworkStatus != NotReachable) {
    [self downloadThumbnailForResource:mediaResource atIndexPath:indexPath];
  }
  mediaCell.delegate = self;
  [mediaCell updateDisplay];
  return mediaCell;
}

- (void)downloadThumbnailForResource:(MediaResource *)resource
                         atIndexPath:(NSIndexPath *)indexPath {
  CDMLogInfo(@"Downloading thumbnail for %@.", resource.name);

  resource.thumbnailLoadTries += 1;
  NSURLSessionDataTask *task = [_thumbnailSession
        dataTaskWithURL:resource.thumbnailURL
      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self didDownloadThumbnailData:data forResource:resource atIndexPath:indexPath];
      }];
  [task resume];
}

- (void)didDownloadThumbnailData:(NSData *)data
                     forResource:(MediaResource *)resource
                     atIndexPath:(NSIndexPath *)indexPath {
  if (data) {
    dispatch_sync(dispatch_get_main_queue(), ^{
      MediaCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
      resource.thumbnailImage = [UIImage imageWithData:data];
      cell.thumbnail = resource.thumbnailImage;
      [cell updateDisplay];
    });
  }
  if (!resource.thumbnailImage) {
    // either there was an error downloading, or the data failed to turn into an image
    // so try to download again
    if (resource.thumbnailLoadTries > 0) {
      // It's tried to load at least once before, so turn on reachability tracking if it was off
      // before in case that's the issue.
      [self startReachabilityTracking];
    }
    if (resource.thumbnailLoadTries < kMaxThumbnailLoadTries) {
      // don't try to load indefinitely; don't want to eat up the user's network plan if the server
      // is down
      [self downloadThumbnailForResource:resource atIndexPath:indexPath];
    } else {
      CDMLogWarn(@"Failed to download thumbnail image for %@, at %@.", resource.name, resource.URL);
    }
  }
}

#pragma mark - Private Methods

- (void)recordingErrorAlert {
  // Display the error.
  NSError *error = [NSError cdmErrorWithCode:CdmPlayeriOSErrorCode_Recording userInfo:nil];
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kAlertErrorRecording
                                                      message:[error localizedFailureReason]
                                                     delegate:nil
                                            cancelButtonTitle:kAlertButtonTitle
                                            otherButtonTitles:nil];
  [alertView show];
}

- (void)connectionErrorAlert:(NSString *)message {
  // Display the error.
  NSError *error = [NSError cdmErrorWithCode:CdmPlayeriOSErrorCode_NoConnection userInfo:nil];
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:message
                                                      message:[error localizedFailureReason]
                                                     delegate:nil
                                            cancelButtonTitle:kAlertButtonTitle
                                            otherButtonTitles:nil];
  [alertView show];
}

- (void)didDeleteMediaURL:(NSURL *)mediaURL
                    error:(NSError *)error
              atIndexPath:(NSIndexPath *)indexPath {
  if (error) {
    CDMLogNSError(error, @"deleting %@", mediaURL);
    return;
  }

  UIAlertView *alert = nil;
  NSString *alertMessage = [NSString
      stringWithFormat:@"Downloaded File Removed.\n File: %@", [mediaURL lastPathComponent]];
  alert = [[UIAlertView alloc] initWithTitle:kAlertTitle
                                     message:alertMessage
                                    delegate:nil
                           cancelButtonTitle:kAlertButtonTitle
                           otherButtonTitles:nil];
  [alert show];
  [self.tableView reloadRowsAtIndexPaths:@[ indexPath ]
                        withRowAnimation:UITableViewRowAnimationNone];
}

- (void)didPressDelete:(MediaCell *)cell {
  NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
  MediaResource *mediaResource = [_mediaResources objectAtIndex:indexPath.row];
  [mediaResource deleteMediaResourceWithCompletionBlock:^(NSError *error) {
    if (!error) {
      // Display a message about the successful deletion.
      [self didDeleteMediaURL:mediaResource.URL error:error atIndexPath:indexPath];
    }
  }];
}

- (void)didPressDownload:(MediaCell *)cell {
  if (self.lastNetworkStatus == ReachableViaWiFi) {  // only download on wifi
    NSInteger row = [self.tableView indexPathForCell:cell].row;
    MediaResource *mediaResource = [_mediaResources objectAtIndex:row];
    [mediaResource downloadMediaResourceWithCompletionBlock:^(NSError *error) {
      if (error) {
        // Start reachability tracking, in case it was a network error.
        [self startReachabilityTracking];

        // Display the error to the user.
        UIAlertView *alert;
        NSString *message =
            [NSString stringWithFormat:@"Failed to download %@\n%@", mediaResource.URL, error];
        alert = [[UIAlertView alloc] initWithTitle:kAlertErrorTitle
                                           message:message
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
      }
    }];
  } else {
    [self connectionErrorAlert:kAlertErrorWifi];
  }
}

- (void)didPressLicense:(MediaCell *)cell {
  NSInteger row = [self.tableView indexPathForCell:cell].row;
  MediaResource *mediaResource = [_mediaResources objectAtIndex:row];
  [mediaResource fetchLicenseInfoWithCompletionBlock:^(NSError *error) {
    if (error) {
      CDMLogNSError(error, @"fetching license for %@", mediaResource.name);
    }
  }];
}

- (void)didPressPlay:(MediaCell *)cell {
  NSInteger row = [self.tableView indexPathForCell:cell].row;
  MediaResource *mediaResource = [_mediaResources objectAtIndex:row];
  if ([RecordingDetector sharedDetector].isRecording) {
    [self recordingErrorAlert];
    return;
  } else if (!mediaResource.isDownloaded && self.lastNetworkStatus == NotReachable) {
    [self connectionErrorAlert:kAlertErrorConnection];
    return;
  }

  DetailViewController *playerViewController =
      [[DetailViewController alloc] initWithMediaResource:mediaResource];
  [[self navigationController] pushViewController:playerViewController animated:YES];
}

@end
