// Copyright 2015 Google Inc. All rights reserved.

#import "PlaylistServer.h"

#import <arpa/inet.h>
#import <ifaddrs.h>
#import <string.h>

#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"
#import "LicenseManager.h"
#import "Logging.h"
#import "PlaylistBuilder.h"
#import "Stream.h"

static int sHttpPort = 8000;
static NSString *const kLocalPlaylist = @"dash2hls.m3u8";
static NSString *const kLocalHost = @"localhost";
static NSString *const kManifestType = @"application/vnd.apple.mpegurl";
static NSString *const kTransportStreamType = @"video/MP2T";

@interface PlaylistServer() <GCDWebServerDelegate>
@end

@implementation PlaylistServer {
  GCDWebServer *_localWebServer;
  int _httpPort;
}

// TODO (theodab): sometimes airplay crashes.
// Specifically, dash_to_hls_api.cc seems to be passing a null value as ts_output when calling
// TransmuxToTS on line 1233.

// Create PlaylistServer object with local IP address if Airplay is off or network IP if on.
- (instancetype)initWithAirplay:(BOOL)isAirplayActive
               licenseServerURL:(NSURL *)licenseServerURL {
  self = [super init];
  if (self) {
    if (licenseServerURL) {
        [LicenseManager sharedInstance].licenseServerURL = licenseServerURL;
     // NSURL *licMgrURL = [LicenseManager sharedInstance].licenseServerURL;
    //  if (licMgrURL && ![licMgrURL isEqual:licenseServerURL]) {
        // Changing License Server URLs in not supported due to the Host being a
        // singleton.
     //   CDMLogError(@"ERROR: Cannot reset existing License Server URL. Using: %@", licMgrURL);
    //  }
    }
    _localWebServer = [[GCDWebServer alloc] init];
    _localWebServer.delegate = self;
    // Random HTTP Port generator.
    // Avoids issues when multiple streams are selected quickly.
    _httpPort = sHttpPort++;

    __weak PlaylistServer *weakSelf = self;
    auto processBlock = ^GCDWebServerResponse *(GCDWebServerRequest *request) {
      return [weakSelf responseForPath:request.path];
    };
    [_localWebServer addDefaultHandlerForMethod:@"GET"
                                   requestClass:[GCDWebServerRequest class]
                                   processBlock:processBlock];

    [_localWebServer startWithOptions:[self gcdOptionsDict:isAirplayActive] error:nullptr];
    _builder = [[PlaylistBuilder alloc] init];
  }
  return self;
}

// Recreates the PlaylistServer object with a different IP address.
- (void)restart:(BOOL)isAirplayActive {
  [_localWebServer stop];
  [_localWebServer startWithOptions:[self gcdOptionsDict:isAirplayActive] error:nullptr];
}

// Makes an option dictionary for GCDWebServer startWithOptions
- (NSDictionary *)gcdOptionsDict:(BOOL)isAirplayActive {
  return @{
    GCDWebServerOption_Port: @(_httpPort),
    GCDWebServerOption_BindToLocalhost: isAirplayActive ? @NO : @YES
  };
}

// Stops the local web server.
- (void)stop {
  // TODO: this should have a callback block saying when it's over, so the master view controller
  // knows it is safe to load up another video

  [_builder destroy];
  _builder = nil;
  // Stopping PlaylistServer is synchronous but stopping GCDWebServer is asynchronous, so unset
  // the delegate to prevent GCDWebServer from referencing a zombie PlaylistServer.
  _localWebServer.delegate = nil;
  [_localWebServer stop];
}

// Tell the builder to begin processing an MPD.
- (void)processMPD:(NSURL *)MPDURL withCompletion:(void (^)(NSError *))completion {
  [_builder processMPD:MPDURL downloadData:YES withCompletion:completion];
}

// Handle HTTP requests for M3U8 and TS files and respond with created data.
- (GCDWebServerResponse *)responseForPath:(NSString *)path {
  NSData *responseData = nil;
  NSString *responseType = nil;
  // Check for incoming filename and return appropriate data.
  if ([path.lastPathComponent isEqualToString:kLocalPlaylist]) {
    CDMLogInfo(@"Requesting %@", path);
    // kDashPlaylist is pre-assigned name in ViewController to ensure variant
    // playlist creation.
    responseData = [_builder.variantPlaylist dataUsingEncoding:NSUTF8StringEncoding];
    responseType = kManifestType;
  } else if ([path.pathExtension isEqualToString:@"m3u8"]) {
    CDMLogInfo(@"Requesting %@", path);
    // Catches children playlist requests and provides the appropriate data in place of m3u8.
    int index = 0;
    if ([self getM3U8Index:&index forURLPath:path]) {
      Stream *stream = _builder.streams[index];
      if (!stream.playlist) {
        CDMLogError(@"stream does not have m3u8");
        return nil;
      }
      if (stream.isLive) {
        // Loop through all streams to ensure all playlists are updated in the event of rate change.
        for (Stream *stream in _builder.streams) {
          stream.playlist = [_builder childPlaylistForStream:stream];
        }
      }
      responseData = [stream.playlist dataUsingEncoding:NSUTF8StringEncoding];
      responseType = kManifestType;
    }
  } else if ([path.pathExtension isEqualToString:@"ts"]) {
    CDMLogInfo(@"Requesting %@", path);
    // Handles individual TS segment requests by transmuxing the source MP4.
    int index = 0;
    int segment = 0;
    if ([self getTSIndex:&index andSegment:&segment forURLPath:path]) {
      responseData = [_builder tsDataForIndex:index segment:segment];
      responseType = kTransportStreamType;
    }
  }
  if (responseData)
    return [GCDWebServerDataResponse responseWithData:responseData contentType:responseType];
  return nil;
}

- (BOOL)getM3U8Index:(int *)index forURLPath:(NSString *)path {
  NSScanner *scanner = [NSScanner scannerWithString:path];
  return ([scanner scanString:@"/" intoString:NULL] && [scanner scanInt:index]);
}

- (BOOL)getTSIndex:(int *)index andSegment:(int *)segment forURLPath:(NSString *)path {
  NSScanner *scanner = [NSScanner scannerWithString:path];
  return ([scanner scanString:@"/" intoString:NULL] && [scanner scanInt:index] &&
          [scanner scanString:@"-" intoString:NULL] && [scanner scanInt:segment] &&
          [scanner scanString:@".ts" intoString:NULL]);
}

#pragma mark - GCDWebServerDelegate

- (void)webServerDidStart:(GCDWebServer *)server {
  _address = server.serverURL.absoluteString;
}

@end
