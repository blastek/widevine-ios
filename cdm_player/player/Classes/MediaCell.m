// Copyright 2015 Google Inc. All rights reserved.

#import "MediaCell.h"

static NSString *const kNormalStateIconName = @"ico_download_before.png";
static NSString *const kDownloadedStateIconName = @"ico_download_after.png";
static CGFloat kImageSize = 40;

@implementation MediaCell {
  UILabel *_downloadPercentLabel;
  UIButton *_licenseButton;
  UIButton *_deleteButton;
  UIButton *_downloadButton;
  UITapGestureRecognizer *_tap;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    _downloadPercentLabel = [[UILabel alloc] init];
    _licenseButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    _downloadButton = [[UIButton alloc] init];
    _deleteButton = [[UIButton alloc] init];
    _licenseButton.hidden = YES;
    // TODO (theodab): handle the "press play" behavior in the table view instead of here
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressPlay)];
    [self.textLabel setUserInteractionEnabled:YES];
    // Set corner radius to arbitrary value based on visual appeal.
    self.imageView.layer.cornerRadius = 4;
    self.imageView.layer.masksToBounds = YES;

    // Add subviews to contentView
    [self.contentView addSubview:_licenseButton];
    [self.contentView addSubview:_downloadButton];
    [self.contentView addSubview:_deleteButton];
    [self.contentView addSubview:_downloadPercentLabel];

    // Format buttons with images
    [_deleteButton setBackgroundImage:[UIImage imageNamed:kDownloadedStateIconName]
                             forState:UIControlStateNormal];
    [_downloadButton setBackgroundImage:[UIImage imageNamed:kNormalStateIconName]
                               forState:UIControlStateNormal];

    // Add button targets
    [_deleteButton addTarget:self
                      action:@selector(didPressDelete)
            forControlEvents:UIControlEventTouchUpInside];
    [_downloadButton addTarget:self
                        action:@selector(didPressDownload)
              forControlEvents:UIControlEventTouchUpInside];
    [_licenseButton addTarget:self
                       action:@selector(didPressLicense)
             forControlEvents:UIControlEventTouchUpInside];

    // Add constraints
    [self setConstraints:_downloadButton
               attribute:NSLayoutAttributeCenterY
                constant:0];
    [self setConstraints:_downloadButton
               attribute:NSLayoutAttributeTrailing
                constant:-5];

    [self setConstraints:_deleteButton
               attribute:NSLayoutAttributeCenterY
                constant:0];
    [self setConstraints:_deleteButton
               attribute:NSLayoutAttributeTrailing
                constant:-5];

    [self setConstraints:_downloadPercentLabel
               attribute:NSLayoutAttributeCenterY
                constant:0];
    [self setConstraints:_downloadPercentLabel
               attribute:NSLayoutAttributeTrailing
                constant:-45];

    [self setConstraints:_licenseButton
               attribute:NSLayoutAttributeCenterY
                constant:0];
    [self setConstraints:_licenseButton
               attribute:NSLayoutAttributeTrailing
                constant:-45];
  }
  return self;
}

- (void)prepareForReuse {
  [super prepareForReuse];
  self.thumbnail = nil;
  [self.textLabel removeGestureRecognizer:_tap];
  [_downloadPercentLabel setHidden:YES];
}

#pragma mark - Load View

- (void)updateDisplay {
  if (!self.imageView.image) {
    [self setThumbnail:_thumbnail];
  }
  _downloadButton.hidden = NO;
  _deleteButton.hidden = NO;
  _downloadPercentLabel.hidden = NO;
  _licenseButton.hidden = NO;
  if (_filesBeingDownloaded.count == 0 && _isDownloaded) {
    // Completed Downloads - Ready for Offline
    [self.textLabel addGestureRecognizer:_tap];
    self.textLabel.textColor = self.offlineColor;
    _downloadButton.hidden = YES;
    _downloadPercentLabel.hidden = YES;
  } else if (_filesBeingDownloaded.count > 0) {
    // Currently being downloaded -- Disable ability to play
    self.textLabel.textColor = [UIColor grayColor];
    _downloadPercentLabel.text = [NSString stringWithFormat:@"%d%%", _percentage];
    _downloadButton.hidden = YES;
    _deleteButton.hidden = YES;
    _licenseButton.hidden = YES;
  } else {
    if (self.canStream) {
      [self.textLabel addGestureRecognizer:_tap];
      self.textLabel.textColor = self.streamingColor;
    } else {
      self.textLabel.textColor = [UIColor grayColor];
    }
    if (!self.canDownload) {
      _downloadButton.hidden = YES;
    }
    _deleteButton.hidden = YES;
    _licenseButton.hidden = YES;
    _downloadPercentLabel.hidden = YES;
  }
  dispatch_async(dispatch_get_main_queue(), ^{
    [self setNeedsLayout];
    [self.contentView setNeedsLayout];
  });
}

- (void)setConstraints:(UIView *)element
             attribute:(NSLayoutAttribute)attribute
              constant:(CGFloat)constant {
  [element setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:element
                                                               attribute:attribute
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.contentView
                                                               attribute:attribute
                                                              multiplier:1.0
                                                                constant:constant]];
}

- (UIColor *)streamingColor {
  return [UIColor colorWithRed:0.0 green:122.0 / 255.0 blue:1.0 alpha:1.0];
}

- (UIColor *)offlineColor {
  return [UIColor colorWithRed:255.0 / 255.0 green:127.0 / 255.0 blue:0.0 alpha:1.0];
}

- (void)setThumbnail:(UIImage *)image {
  // Resize and scale downloaded image, then add to ImageView.
  // Size set to cell height.
  // TODO(seawardt): Change to use CoreGraphics
  if (image) {
    CGSize size = CGSizeMake(kImageSize, kImageSize);
    UIGraphicsBeginImageContextWithOptions(size, NO, UIScreen.mainScreen.scale);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.imageView.image = thumbnail;
  } else {
    self.imageView.image = nil;
  }
}

#pragma mark - Player Methods

- (void)didPressDelete {
  [_delegate didPressDelete:self];
}

- (void)didPressDownload {
  [_delegate didPressDownload:self];
}

- (void)didPressLicense {
  [_delegate didPressLicense:self];
}

- (void)didPressPlay {
  [_delegate didPressPlay:self];
}

@end
