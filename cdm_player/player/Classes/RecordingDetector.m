// Copyright 2016 Google Inc. All rights reserved.

#import "RecordingDetector.h"
#import <UIKit/UIKit.h>

float const kCheckTimerInterval = 0.25;
NSString *kRecordingStatusChangedNotification = @"RecordingStatusChangedNotification";

@interface RecordingDetector()

@property BOOL lastIsRecording;
@property NSTimer *checkTimer;

@end

@implementation RecordingDetector

+ (instancetype)sharedDetector {
  static RecordingDetector *sharedDetector = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedDetector = [[self alloc] init];
  });
  return sharedDetector;
}

+ (void)stopChecking {
  RecordingDetector *detector = [RecordingDetector sharedDetector];
  if (detector.checkTimer) {
    [detector.checkTimer invalidate];
    detector.checkTimer = NULL;
  }
}

+ (void)startChecking {
  // This timer will stop working after a while if put in the background, so call this again upon
  // returning from the background.
  RecordingDetector *detector = [RecordingDetector sharedDetector];
  if (detector.checkTimer)
    [self stopChecking];

  detector.checkTimer = [NSTimer scheduledTimerWithTimeInterval:kCheckTimerInterval
                                                         target:detector
                                                       selector:@selector(checkTimerTick:)
                                                       userInfo:nil
                                                        repeats:YES];
}

- (void)checkTimerTick:(NSTimer * _Nonnull)timer {
  BOOL isRecording = [self isRecording];
  if (isRecording != self.lastIsRecording) {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:kRecordingStatusChangedNotification object:nil];
  }
  self.lastIsRecording = isRecording;
}

- (id)init {
  if (self = [super init]) {
    self.lastIsRecording = NO;
    self.checkTimer = NULL;
  }
  return self;
}

- (BOOL)isRecording {
  // Note that this will detect not just screen recording, but also airplay mirroring.
  // TODO (theodab): test if this triggers during non-mirroring airplay, e.g. video mirroring...
  for (UIScreen *screen in UIScreen.screens) {
    if ([screen respondsToSelector:@selector(isCaptured)]) {
      // The screen has an isCaptured method, so this is running under an iOS version with screen
      // recording.
      if ([screen performSelector:@selector(isCaptured)])
        return YES;
    } else {
      // This is for a version of iOS without screen recording; just check for screen mirroring,
      // for consistency with the iOS 11 version.
      if (screen.mirroredScreen)
        return YES;
    }
  }
  return NO;
}

@end
