// Copyright 2015 Google Inc. All rights reserved.

#import "MpdParser.h"

#import <objc/message.h>
#import <objc/runtime.h>
#import "CdmPlayerHelpers.h"
#import "Logging.h"

static NSString *const kDASHAdaptationSet = @"AdaptationSet";
static NSString *const kDASHContentComponent = @"ContentComponent";
static NSString *const kDASHContentProtection = @"ContentProtection";
static NSString *const kDASHPeriod = @"Period";
static NSString *const kDASHRepresentation = @"Representation";
static NSString *const kDASHRepresentationBaseURL = @"BaseURL";
static NSString *const kDASHRepresentationBW = @"bandwidth";
static NSString *const kDASHRepresentationCodec = @"codecs";
static NSString *const kDASHRepresentationHeight = @"height";
static NSString *const kDASHRepresentationMime = @"mimeType";
static NSString *const kDASHRepresentationWidth = @"width";
static NSString *const kDASHSegment = @"Segment";
static NSString *const kDASHSegmentBase = @"SegmentBase";
static NSString *const kDASHSegmentBaseIndexRange = @"indexRange";
static NSString *const kDASHSegmentBaseInitializationRange = @"range";
static NSString *const kDASHSegmentInitRange = @"Initialization";
static NSString *const kDASHSegmentList = @"SegmentList";
static NSString *const kDASHSegmentListURL = @"SegmentURL";
static NSString *const kDASHSegmentTemplate = @"SegmentTemplate";
static NSString *const kDASHSegmentTimeline = @"SegmentTimeline";
static NSString *const kDASHTimelineDuration = @"d";
static NSString *const kDASHTimelineRepeat = @"r";
static NSString *const kDASHTimelineSegment = @"S";
static NSString *const kDASHTimelineTime = @"t";


static NSString *const kAttrAudioSampleRate = @"audioSamplingRate";
static NSString *const kAttrAvailbleStartTime = @"availabilityStartTime";
static NSString *const kAttrBandwidth = @"bandwidth";
static NSString *const kAttrCodecs = @"codecs";
static NSString *const kAttrCodecAvc1 = @"avc1";
static NSString *const kAttrCodecMp4a = @"mp4a";
static NSString *const kAttrContentType = @"contentType";
static NSString *const kAttrDuration = @"duration";
static NSString *const kAttrHeight = @"height";
static NSString *const kAttrId = @"id";
static NSString *const kAttrIndexRange = @"indexRange";
static NSString *const kAttrInitialization = @"initialization";
static NSString *const kAttrLang = @"lang";
static NSString *const kAttrMedia = @"media";
static NSString *const kAttrMimeType = @"mimeType";
static NSString *const kAttrMimeTypeAudio = @"audio/";
static NSString *const kAttrMimeTypeVideo = @"video/";
static NSString *const kAttrMinBufferTime = @"minBufferTime";
static NSString *const kAttrMinUpdatePeriod = @"minimumUpdatePeriod";
static NSString *const kAttrNumChannels = @"numChannels";
static NSString *const kAttrPSSH = @"PSSH";
static NSString *const kAttrPSSHCenc = @"cenc:pssh";
static NSString *const kAttrSampleRate = @"sampleRate";
static NSString *const kAttrStartNumber = @"startNumber";
static NSString *const kAttrTimeShiftBuffer = @"timeShiftBufferDepth";
static NSString *const kAttrTimescale = @"timescale";
static NSString *const kAttrWidth = @"width";

static NSString *const kBoolNo = @"NO";
static NSString *const kBoolYes = @"YES";
static NSString *const kDASHMediaType = @"DASHMediaType";
static NSString *const kDASHToHlsString = @"DashToHls";
static NSString *const kDASHSeparator = @"-";
static NSString *const kHTTPString = @"http";
static NSString *const kNewLineCharacter = @"\n";
static NSString *const kRepresentationIDPlaceholder = @"$RepresentationID$";
static NSString *const kRootURL = @"rootURL";
static NSString *const kSlashesString = @"//";
static NSString *const kStreamingString = @"PlaylistBuilder";
static NSString *const kPlaylistString = @"playlist";
static NSString *const kVideoString = @"video";
static NSString *const kRegexPattern =
    @"^P(?:(\\d{0,2})Y)?(?:(\\d{0,2})M)?(?:(\\d{0,2})D)"
    @"?.(?:(\\d{0,2})H)?(?:(\\d{0,2})M)?(?:(\\d*[.]?\\d+)S)?$";

@implementation MPDParser {
  NSString *_currentElement;
  NSDateFormatter *_dateFormat;
  NSMutableArray *_durations;
  NSUInteger _maxAudioBandwidth;
  NSUInteger _maxVideoBandwidth;
  NSMutableDictionary *_MPDDict;
  NSURL *_MPDURL;
  NSInteger _offlineAudioIndex;
  NSInteger _offlineVideoIndex;
  NSXMLParser *_parser;
  BOOL _playOffline;
  NSRegularExpression *_regex;
  BOOL _storeOffline;
  NSInteger _streamCount;
  PlaylistBuilder *_playlistBuilder;
}

// Init methods.
- (instancetype)initWithMPDData:(NSData *)MPDData {
  return [self initWithPlaylistBuilder:nil MPDData:MPDData baseURL:nil storeOffline:NO];
}

- (instancetype)initWithPlaylistBuilder:(PlaylistBuilder *)playlistBuilder
                                MPDData:(NSData *)MPDData
                                baseURL:(NSURL *)baseURL
                           storeOffline:(BOOL)storeOffline {
  self = [super init];
  if (self) {
    _MPDDict = [[NSMutableDictionary alloc] init];
    _parser = [[NSXMLParser alloc] initWithData:MPDData];
    _MPDURL = baseURL;
    _durations = [[NSMutableArray alloc] init];
    _streams = [[NSMutableArray alloc] init];
    _playlistBuilder = playlistBuilder;
    _storeOffline = storeOffline;
    // Only files in the "Documents" path are considered offline files, for testing purposes.
    _playOffline = [_MPDURL isFileURL] && [_MPDURL.pathComponents containsObject:@"Documents"];
    if (_parser) {
      [_parser setDelegate:self];
      [_parser parse];
    }
  }
  return self;
}

// External methods to be used to begin parsing.
+ (NSArray *)streamArrayFromMPD:(PlaylistBuilder *)playlistBuilder
                        MPDData:(NSData *)MPDData
                        baseURL:(NSURL *)baseURL
                   storeOffline:(BOOL)storeOffline {
  // TODO(seawardt): Implement as a class method of Stream.
  return [[MPDParser alloc] initWithPlaylistBuilder:playlistBuilder
                                            MPDData:MPDData
                                            baseURL:baseURL
                                       storeOffline:storeOffline]
    .streams;
}

#pragma mark NSXMLParser methods -- start

// Set stream counter to track the amout of streams found in the manifest.
- (void)parserDidStartDocument:(NSXMLParser *)parser {
  _streamCount = 0;
}

// XML Element found, being parsing.
- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
     attributes:(NSDictionary *)attributeDictionary {
  _currentElement = elementName;
  if ([elementName hasPrefix:kDASHSegment]) {
    if (![self setDASHMediaType:elementName]) {
      [parser abortParsing];
    }
  }
  NSMutableDictionary *durationDictionary = [[NSMutableDictionary alloc] init];
  for (NSString *key in attributeDictionary) {
    NSString *value = [[attributeDictionary valueForKey:key]
                       stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([elementName isEqualToString:kDASHContentProtection]) {
      // Reset PSSH for each Adapation Set.
      [_MPDDict removeObjectForKey:kAttrPSSHCenc];
    }
    if ([elementName isEqualToString:kDASHTimelineSegment]) {
      [durationDictionary setValue:value forKey:key];
    } else {
      // Check that value isnt blank.
      if (([value length] != 0)) {
        [_MPDDict setValue:value forKey:key];
        if ([key isEqualToString:kDASHRepresentationMime]) {
          if ([value containsString:kVideoString]) {
            [_MPDDict setValue:kBoolYes forKey:kVideoString];
          } else {
            [_MPDDict setValue:kBoolNo forKey:kVideoString];
          }
        }
      }
    }
  }
  if ([[durationDictionary allKeys] count] > 0) {
    [_durations addObject:durationDictionary];
  }
}

// Characters found within an element that do not contain a specific key.
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
  if (string) {
    // Ignores values that contain two lines. If needed, modify as necessary.
    if (![string containsString:kNewLineCharacter]) {
      if ([string hasPrefix:kSlashesString]) {
        [_MPDDict setValue:string forKey:kRootURL];
      }
      // Stores PSSH to dictionary
      if ([_currentElement isEqualToString:kAttrPSSHCenc]) {
        [_MPDDict setValue:string forKey:kAttrPSSH];
      }
      [_MPDDict setValue:string forKey:_currentElement];
    }
  }
}

// Finished parsing Element.
- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName {
  if ([elementName isEqualToString:kDASHRepresentation]) {
    // Setup stream if mimeType (video/mp4) and codec (avc1) are supported.
    if ([[_MPDDict objectForKey:kAttrMimeType] containsString:kAttrMimeTypeVideo]) {
      if ([[_MPDDict objectForKey:kAttrCodecs] containsString:kAttrCodecAvc1]) {
        if (![self setStreamProperties:elementName]) {
          [parser abortParsing];
        }
      }
    }
    // Setup stream if mimeType (audio/mp4) and codec (mp4a) are supported.
    if ([[_MPDDict objectForKey:kAttrMimeType] containsString:kAttrMimeTypeAudio]) {
      if ([[_MPDDict objectForKey:kAttrCodecs] containsString:kAttrCodecMp4a]) {
        if (![self setStreamProperties:elementName]) {
          [parser abortParsing];
        }
      }
    }
  }
}

// If parsing fails, this error will be returned.
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
  CDMLogNSError(parseError, @"parsing");
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
}

#pragma mark NSXMLParser methods -- end

// Look up aviailable properties from Stream object and populate the required values.
- (BOOL)setStreamProperties:(NSString *)elementName {
  Stream *stream = [[Stream alloc] initWithPlaylistBuilder:_playlistBuilder];
  unsigned int numberOfProperties = 0;
  unsigned int propertyIndex = 0;
  objc_property_t *properties = class_copyPropertyList([Stream class], &numberOfProperties);
  for (propertyIndex = 0; propertyIndex < numberOfProperties; propertyIndex++) {
    objc_property_t property = properties[propertyIndex];
    [self setProperty:stream property:property];
  }
  free(properties);
  // Stream Complete
  // TODO: Enable _storeOffline by downloading single video/audio stream (b/27264914)
  _storeOffline = NO;
  if (_storeOffline) {
    [self storeOfflineStream:stream];
  } else {
    [_streams addObject:stream];
    _streamCount++;
  }
  if ([self validateStreamAttributes:stream]) {
    return YES;
  }
  return NO;
}

// Check Property type and determine attribute.
- (NSString *)getPropertyType:(objc_property_t)property {
  const char *attribute = property_getAttributes(property);
  if (!attribute) {
    CDMLogError(@"empty property attribute %@", property);
    return nil;
  }
  NSString *attributeString = [NSString stringWithUTF8String:attribute];
  NSArray *attributeArray = [attributeString componentsSeparatedByString:@","];
  NSString *attributeSubstr = [[attributeArray objectAtIndex:0] substringFromIndex:1];
  NSString *attributeStripped = [attributeSubstr stringByReplacingOccurrencesOfString:@"\""
                                                                           withString:@""];

  // Verify attribute values are valid.
  if (!attributeStripped || [attributeStripped length] == 0) {
    CDMLogError(@"empty property character %@", property);
    return nil;
  }

  switch (attribute[1]) {
    case '@':  // NSString
      attributeString = [attributeStripped substringFromIndex:1];
      break;
    case 'i':  // NSInteger 32-bit
    case 'q':  // NSInteger 64-bit
      attributeString = @"NSInteger";
      break;
    case 'c':  // BOOL 32-bit
    case 'B':  // BOOL 64-bit
      attributeString = @"BOOL";
      break;
    case '^':  // Special Class
      attributeString =
          [[[attributeStripped substringFromIndex:2] componentsSeparatedByString:@"="] firstObject];
      break;
    case '{':
      attributeString = @"NSRange";
      break;
    default: {
      CDMLogError(@"no property matched %c", attribute[1]);
      attributeString = nil;
      break;
    }
  }
  return attributeString;
}

// Uses available property in Stream object and value when property and key match.
- (void)setProperty:(Stream *)stream property:(objc_property_t)property {
  NSString *propertyName = [NSString stringWithUTF8String:property_getName(property)];
  const char *attribute = property_getAttributes(property);
  NSString *attributeString = [NSString stringWithUTF8String:attribute];
  NSArray *attributeArray = [attributeString componentsSeparatedByString:@","];
  NSString *attributeSubstr = [[attributeArray objectAtIndex:0] substringFromIndex:1];
  NSString *attributeStripped = [attributeSubstr stringByReplacingOccurrencesOfString:@"\""
                                                                           withString:@""];
  NSString *propertyType = [attributeStripped substringFromIndex:1];
  switch (attribute[1]) {
    case '@':  // Char
      if ([propertyType isEqualToString:@"NSURL"]) {
        NSURL *value =
            [self makeStreamURL:[_MPDDict objectForKey:kDASHRepresentationBaseURL] init:NO];
        [stream setValue:value forKey:propertyName];
      }
      if ([propertyType isEqualToString:@"NSData"]) {
        NSData *value = [[NSData alloc] init];
        if ([propertyName isEqualToString:kAttrPSSH]) {
          NSString *PSSHString = [_MPDDict objectForKey:kAttrPSSHCenc];
          if (PSSHString) {
            value = [[NSData alloc] initWithBase64EncodedString:PSSHString options:0];
          }
        }
        [stream setValue:value forKey:propertyName];
      }
      if ([propertyType isEqualToString:@"NSString"]) {
        NSString *value = [_MPDDict objectForKey:propertyName];
        [stream setValue:value forKey:propertyName];
      }
      if ([propertyType isEqualToString:@"LiveStream"]) {
        [self setLiveProperties:(Stream *)stream];
      }
      if ([propertyType isEqualToString:@"NSDate"]) {
        [stream setValue:[NSDate dateWithTimeIntervalSince1970:0] forKey:propertyName];
      }
      break;
    case 'i':  // NSInteger 32-bit
    case 'q':  // NSInteger 64-bit
      if ([propertyName isEqualToString:@"streamIndex"]) {
        [stream setValue:[NSNumber numberWithInteger:_streamCount] forKey:propertyName];
      } else if ([propertyName isEqualToString:@"mediaPresentationDuration"]) {
        NSUInteger value = [self convertDurationToSeconds:[_MPDDict objectForKey:propertyName]];
        [stream setValue:[NSNumber numberWithInteger:value] forKey:propertyName];
      } else {
        NSUInteger value = [[_MPDDict objectForKey:propertyName] integerValue];
        [stream setValue:[NSNumber numberWithInteger:value] forKey:propertyName];
      }
      break;
    case 'c':  // BOOL 32-bit
    case 'B':  // BOOL 64-bit
      if (propertyName) {
        BOOL value = [[_MPDDict objectForKey:propertyName] boolValue];
        [stream setValue:[NSNumber numberWithBool:value] forKey:propertyName];
      }
      break;
    case '^':  // Special Class
      break;
    case '{':
      if ([propertyType isEqualToString:@"_NSRange=QQ}"]) {
        NSRange value = [self extractInitialRange];
        stream.initialRange = value;
      }
      break;
    default: {
      CDMLogError(@"unknown property type %c", attribute[1]);
      attributeString = nil;
      break;
    }
  }
}

// Check each stream and store highest rates for video/audio.
- (void)storeOfflineStream:(Stream *)stream {
  // Look up higest bitrate
  if (stream.isVideo) {
    if (stream.bandwidth > _maxVideoBandwidth) {
      if (_maxVideoBandwidth) {
        [_streams replaceObjectAtIndex:_offlineVideoIndex withObject:stream];
      } else {
        [_streams addObject:stream];
        _offlineVideoIndex = _streamCount;
      }
      _maxVideoBandwidth = stream.bandwidth;
    }
  } else {
    if (stream.bandwidth > _maxAudioBandwidth) {
      if (_maxAudioBandwidth) {
        [_streams replaceObjectAtIndex:_offlineAudioIndex withObject:stream];
      } else {
        [_streams addObject:stream];
        _offlineAudioIndex = _streamCount;
      }
      _maxAudioBandwidth = stream.bandwidth;
    }
  }
}

// Ensure all properties are populated from the MPD, otherwise return false.
- (BOOL)validateStreamAttributes:(Stream *)stream {
  BOOL attributeExists = YES;
  unsigned int numberOfProperties, i;
  objc_property_t *properties = class_copyPropertyList([Stream class], &numberOfProperties);
  for (i = 0; i < numberOfProperties; i++) {
    objc_property_t property = properties[i];
    const char *propName = property_getName(property);
    if (propName) {
      NSString *propertyName = [NSString stringWithUTF8String:propName];
      NSString *propertyType = [self getPropertyType:property];
      BOOL doNotCheck =
          [propertyType containsString:kDASHToHlsString] ||
          [propertyName isEqualToString:kAttrPSSH] ||
          [propertyType isEqualToString:kStreamingString] ||
          [propertyName isEqualToString:kPlaylistString];
      if (!doNotCheck) {
        NSString *propertyValue = [stream valueForKey:propertyName];
        if (!propertyValue) {
          CDMLogWarn(@"Property of type %@ not set for stream %@", propertyType, propertyName);
          attributeExists = NO;
        }
      }
    }
  }
  free(properties);
  return attributeExists;
}

// Determine what type of DASH Template is being used.
- (BOOL)setDASHMediaType:(NSString *)elementName {
  // SegmentBase
  if ([elementName isEqualToString:kDASHSegmentBase]) {
    [_MPDDict setValue:@(DASHMediaTypeSegmentBase) forKey:kDASHMediaType];
  }
  // SegmentList w/Duration
  if ([elementName isEqualToString:kDASHSegmentList]) {
    [_MPDDict setValue:@(DASHMediaTypeSegmentListDuration) forKey:kDASHMediaType];
  }
  // SegmentTemplate w/Duration
  if ([elementName isEqualToString:kDASHSegmentTemplate]) {
    [_MPDDict setValue:@(DASHMediaTypeSegmentTemplateDuration) forKey:kDASHMediaType];
  }
  // SegmentTimeline
  if ([elementName isEqualToString:kDASHSegmentTimeline]) {
    // SegmentTimeline is sub-section to SegmentList
    if ([[_MPDDict valueForKey:kDASHMediaType] isEqual:@(DASHMediaTypeSegmentListDuration)]) {
      [_MPDDict setValue:[NSNumber numberWithUnsignedInteger:DASHMediaTypeSegmentListTimeline]
                  forKey:kDASHMediaType];
    }
    // SegmentTimeline is sub-section to SegmentTemplate
    if ([[_MPDDict valueForKey:kDASHMediaType] isEqual:@(DASHMediaTypeSegmentTemplateDuration)]) {
      [_MPDDict setValue:[NSNumber numberWithUnsignedInteger:DASHMediaTypeSegmentTemplateTimeline]
                  forKey:kDASHMediaType];
      [_durations removeAllObjects];
    }
  }
  if ([_MPDDict objectForKey:kDASHMediaType]) {
    return YES;
  }
  // DASHMediaType has not been set.
  return NO;
}

// Extract ranges from Dictionary, then parse and create Initialization Range.
- (NSRange)extractInitialRange {
  NSString *range = [_MPDDict objectForKey:kDASHSegmentBaseInitializationRange];
  [_MPDDict removeObjectForKey:kDASHSegmentBaseInitializationRange];
  NSArray *rangeValues = [range componentsSeparatedByString:kDASHSeparator];
  NSInteger startRange = [rangeValues[0] intValue];
  NSString *indexRange = [_MPDDict objectForKey:kAttrIndexRange];
  [_MPDDict removeObjectForKey:kAttrIndexRange];
  NSArray *indexRangeValues = [indexRange componentsSeparatedByString:kDASHSeparator];

  NSInteger length = 0;
  if (indexRangeValues && rangeValues) {
    // Add 1 to avoid overlap in bytes to the length.
    length = [indexRangeValues[1] intValue] + 1;
    if (startRange >= length) {
      CDMLogError(@"start range %zd is greater than length %zd", startRange, length);
      return NSMakeRange(0, 0);
    }
    if (length == 0) {
      CDMLogError(@"length %zd is not valid", length);
    }
  }
  NSRange initialRange = NSMakeRange(startRange, length);
  return initialRange;
}

// Parse MPEG DASH duration format and return seconds.
// https://en.wikipedia.org/wiki/ISO_8601#Durations
// TODO(seawardt): Implement support for Leap year and months that are not 30 days.
- (NSUInteger)convertDurationToSeconds:(NSString *)string {
  if (!string) {
    return 0;
  }
  NSUInteger duration = 0;
  NSRange searchRange = NSMakeRange(0, [string length]);

  int years = 0;
  int months = 0;
  int days = 0;
  int hours = 0;
  int minutes = 0;
  float seconds = 0;
  if (!_regex) {
    _regex = [NSRegularExpression regularExpressionWithPattern:kRegexPattern
                                                       options:0
                                                         error:nil];
  }
  NSTextCheckingResult *match =
      [_regex firstMatchInString:string options:0 range:searchRange];
  if (match) {
    NSRange matchGroup1 = [match rangeAtIndex:1];
    NSRange matchGroup2 = [match rangeAtIndex:2];
    NSRange matchGroup3 = [match rangeAtIndex:3];
    NSRange matchGroup4 = [match rangeAtIndex:4];
    NSRange matchGroup5 = [match rangeAtIndex:5];
    NSRange matchGroup6 = [match rangeAtIndex:6];
    years = matchGroup1.length > 0 ? [[string substringWithRange:matchGroup1] intValue] : 0;
    months = matchGroup2.length > 0 ? [[string substringWithRange:matchGroup2] intValue] : 0;
    days = matchGroup3.length > 0 ? [[string substringWithRange:matchGroup3] intValue] : 0;
    hours = matchGroup4.length > 0 ? [[string substringWithRange:matchGroup4] intValue] : 0;
    minutes = matchGroup5.length > 0 ? [[string substringWithRange:matchGroup5] intValue] : 0;
    seconds = matchGroup6.length > 0 ? [[string substringWithRange:matchGroup6] floatValue] : 0;
  }
  duration = (60 * 60 * 24 * 365) * years + (60 * 60 * 24 * 30) * months + (60 * 60 * 24) * days +
             (60 * 60) * hours + 60 * minutes + seconds;
  return duration;
}

// Builds Stream.LiveStream object. May be used for Non-Live streams depending on Manifest.
- (void)setLiveProperties:(Stream *)stream {
  if (![_MPDDict objectForKey:kDASHMediaType]) {
    // MediaType is unset. Skipping and assuming on-demand.
    return;
  }
  if ([[_MPDDict objectForKey:kDASHMediaType] isEqual:@(DASHMediaTypeSegmentBase)]) {
    // Ignore if SegmentBase manifest is being used.
    return;
  }

  LiveStream *liveStream = stream.liveStream;
  NSString *availableStartTimeString = _MPDDict[kAttrAvailbleStartTime];
  // Check if Available Start Time exists in manifest, then changes to NSDate.
  if (availableStartTimeString) {
    if (!_dateFormat) {
      _dateFormat = [[NSDateFormatter alloc] init];
      [_dateFormat setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
      [_dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    stream.liveStream.availabilityStartTime = [_dateFormat dateFromString:availableStartTimeString];
  }
  liveStream.duration = [_MPDDict[kAttrDuration] integerValue];
  liveStream.initializationURL = [self makeStreamURL:_MPDDict[kAttrInitialization]
                                                init:YES];
  liveStream.mediaFileName = _MPDDict[kAttrMedia];
  liveStream.minBufferTime = [self convertDurationToSeconds:_MPDDict[kAttrMinBufferTime]];
  liveStream.minimumUpdatePeriod = [_MPDDict[kAttrMinUpdatePeriod]integerValue];
  liveStream.representationId = _MPDDict[kAttrId];
  liveStream.startNumber = [_MPDDict[kAttrStartNumber] integerValue];
  liveStream.timescale = [_MPDDict[kAttrTimescale] integerValue];
  liveStream.timeShiftBufferDepth = [_MPDDict[kAttrTimeShiftBuffer] integerValue];
  liveStream.segmentDuration = (float)liveStream.duration / (float)liveStream.timescale;
  if ([[_MPDDict objectForKey:kDASHMediaType] isEqual:@(DASHMediaTypeSegmentTemplateTimeline)]) {
    liveStream.durationArray = [[NSMutableArray alloc] init];
    NSInteger timestamp = 0;
    for (NSDictionary *dict in _durations) {
      if ([dict objectForKey:kDASHTimelineTime]) {
        timestamp = [[dict valueForKey:kDASHTimelineTime] integerValue];
        liveStream.startNumber = timestamp / liveStream.timescale;
      }
      if ([dict objectForKey:kDASHTimelineDuration]) {
        NSInteger currentTimestamp = [[dict valueForKey:kDASHTimelineDuration] integerValue];
        if ((currentTimestamp / liveStream.timescale) > liveStream.segmentDuration) {
          liveStream.segmentDuration = (currentTimestamp / liveStream.timescale);
        }
        timestamp = currentTimestamp + timestamp;
        [liveStream.durationArray addObject:[@(timestamp) stringValue]];
      }
      if ([dict objectForKey:kDASHTimelineRepeat]) {
        int repeat = [[dict valueForKey:kDASHTimelineRepeat] intValue];
        for (uint64_t count = 0; count < repeat; ++count) {
          timestamp = [[dict valueForKey:kDASHTimelineDuration] integerValue] + timestamp;
          [liveStream.durationArray addObject:[@(timestamp) stringValue]];
        }
      }
    }
  }
}

// Adds a complete URL for each stream.
- (NSURL *)makeStreamURL:(NSString *)URLString init:(BOOL)init {
  // BaseURL and Initialization URLs were not found. Use media URL then.
  if (!URLString) {
    URLString = [_MPDDict objectForKey:kAttrMedia];
  }
  if (_playOffline) {
    return CDMDocumentFileURLForFilename(URLString.lastPathComponent, _MPDURL.lastPathComponent);
  }
  if (init) {
    URLString = [URLString stringByReplacingOccurrencesOfString:kRepresentationIDPlaceholder
                                                         withString:[_MPDDict valueForKey:kAttrId]];
  } else if ([_MPDDict objectForKey:kAttrMedia]) {
    URLString = [_MPDDict objectForKey:kAttrMedia];
  }
  // URL is already complete. Move on.
  if ([URLString containsString:kHTTPString]) {
    return [[NSURL alloc] initWithString:URLString];
  }
  NSString *rootURL = [_MPDDict objectForKey:kRootURL];
  if (rootURL) {
    // The root URL can start with // as opposed to a scheme, appends http(s)
    if ([rootURL rangeOfString:kHTTPString].location == NSNotFound) {
      rootURL = [_MPDURL.scheme stringByAppendingFormat:@":%@", rootURL];
    }
    // Removes trailing path component (if present).
    NSURL *URL = [[NSURL alloc] initWithString:rootURL];
    if ([URL pathExtension]) {
      URL = [URL URLByDeletingLastPathComponent];
    }
    return [URL URLByAppendingPathComponent:URLString];
  }
  // Removes query arguments to properly append the last component path.
  NSURLComponents *URLComponents =
      [[NSURLComponents alloc] initWithURL:_MPDURL resolvingAgainstBaseURL:YES];
  URLComponents.query = nil;
  URLComponents.fragment = nil;
  _MPDURL = URLComponents.URL;
  if (init) {
    URLString = [URLString stringByReplacingOccurrencesOfString:kRepresentationIDPlaceholder
                                                     withString:[_MPDDict valueForKey:kAttrId]];
  }
  return [[_MPDURL URLByDeletingLastPathComponent] URLByAppendingPathComponent:URLString];
}

// Offline usage to delete files listed in MPD.
+ (void)deleteFilesInMPD:(NSURL *)MPDURL {
  NSData *MPDData = [NSData dataWithContentsOfURL:MPDURL];
  if (!MPDData) {
    CDMLogError(@"no MPDData for %@", MPDURL);
    return;
  }
  NSError *error = nil;
  NSArray *remoteURLs =
      [MPDParser streamArrayFromMPD:nil MPDData:MPDData baseURL:MPDURL storeOffline:YES];
  NSFileManager *defaultFileManager = [NSFileManager defaultManager];
  for (Stream *stream in remoteURLs) {
    NSURL *fileURL = CDMDocumentFileURLForFilename(stream.sourceURL.lastPathComponent,
                                                   MPDURL.lastPathComponent);
    [defaultFileManager removeItemAtURL:fileURL error:&error];
    if (error) {
      CDMLogNSError(error, @"deleting existing file at %@", fileURL);
      return;
    }
    CDMLogInfo(@"Deleting %@", fileURL);
  }
  [defaultFileManager removeItemAtURL:MPDURL error:nil];
}

@end
