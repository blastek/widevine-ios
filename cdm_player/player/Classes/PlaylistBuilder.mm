// Copyright 2015 Google Inc. All rights reserved.

#import "PlaylistBuilder.h"

#import "DashToHlsApiAVFramework.h"
#import "Downloader.h"
#import "Logging.h"
#import "MpdParser.h"

NSString *kLocalPlaylistReadyNotification = @"LocalPlaylistReadyNotificaiton";

@implementation PlaylistBuilder {
  NSUInteger _currentAudioSegment;
  NSUInteger _currentVideoSegment;
  NSUInteger _numLoadingStreams;
  dispatch_group_t _playlistBuilderGroup;
  dispatch_queue_t _playlistBuilderQ;
  BOOL _shouldDestroy;
}

static NSString *const kNumberPlaceholder = @"$Number";
static NSString *const kTimePlaceholder = @"$Time$";
// Regex to look for $Number$ or $Number<number padding>$
static NSString *const kNumberRegexPattern = @"\\$Number(%[^$]+)?\\$";
static NSString *const kNumberFormat = @"%d";

static NSString *kAudioPlaylistFormat =
    @"#EXT-X-MEDIA:URI=\"%d.m3u8\",TYPE=AUDIO,GROUP-ID=\"audio\",NAME=\"audio%"
    @"d\","
    @"DEFAULT=%@,AUTOSELECT=YES\n";
static NSString *kAudioSegmentFormat = @"#EXTINF:%0.06f,\n%d-%d.ts\n";

static NSString *const kDynamicPlaylistHeader = @"#EXTM3U\n"
                                                @"#EXT-X-VERSION:3\n"
                                                @"#EXT-X-MEDIA-SEQUENCE:%d\n"
                                                @"#EXT-X-TARGETDURATION:%d\n";

static NSString *const kPlaylistVOD = @"#EXTM3U\n"
                                      @"#EXT-X-VERSION:3\n"
                                      @"#EXT-X-MEDIA-SEQUENCE:%d\n"
                                      @"#EXT-X-TARGETDURATION:%llu\n";

static NSString *const kDiscontinuity = @"#EXT-X-DISCONTINUITY\n";
static NSString *const kPlaylistVODEnd = @"#EXT-X-ENDLIST";

static NSString *kVariantPlaylist = @"#EXTM3U\n#EXT-X-VERSION:3\n";

static NSString *kVideoPlaylistFormat =
    @"#EXT-X-STREAM-INF:BANDWIDTH=%lu,CODECS=\"%@\",RESOLUTION=%.0lux%.0lu,"
    @"AUDIO=\"audio\""
    @"\n%d.m3u8\n";

static NSString *kVideoSegmentFormat = @"#EXTINF:%0.06f,\n%d-%d.ts\n";

static NSString *kLiveBandwidth = @"$Bandwidth$";
static NSString *kLiveNumber = @"$Number$";
static NSString *kLiveRepresentationID = @"$RepresentationID$";

- (instancetype)init {
  if (self = [super init]) {
    _playlistBuilderGroup = dispatch_group_create();
    _playlistBuilderQ =
        dispatch_queue_create("com.google.widevine.cdm-ref-player.PlaylistBuilder", NULL);
    _streams = [NSMutableArray array];
    _numLoadingStreams = 0;
    _shouldDestroy = NO;
  }
  return self;
}

-(void)dealloc {
  for (Stream *stream in _streams) {
    DashToHlsStatus status = Udt_ReleaseSession(stream.session);

    if (status != kDashToHlsStatus_OK) {
      CDMLogError(@"Unable to release Stream: %@", stream.session);
    }
  }
}

// Downloads MPD and creates HLS Playlists.
- (void)downloadAndParseMPD:(NSURL *)MPDURL
        withCompletionBlock:(void (^)(NSArray<Stream *> *streams, NSError *error))completion {
  NSParameterAssert(MPDURL);
  CDMLogInfo(@"Processing %@", MPDURL);
  dispatch_group_async(_playlistBuilderGroup, _playlistBuilderQ, ^{
    NSError *error = nil;
    NSData *MPDData =
        [NSData dataWithContentsOfURL:MPDURL options:NSDataReadingUncached error:&error];
    if (error) {
      completion(nil, error);
    } else {
      _streams = [MPDParser streamArrayFromMPD:self MPDData:MPDData baseURL:MPDURL storeOffline:NO];
      _numLoadingStreams = _streams.count;
      _variantPlaylist = [self variantPlaylistForStreams:_streams];
      completion(_streams, nil);
    }
  });
}

- (void)processMPD:(NSURL *)MPDURL
      downloadData:(BOOL)downloadData
    withCompletion:(void (^)(NSError *))completion {
  auto wrappedCompletion = ^(NSArray<Stream *> *streams, NSError *error) {
    if (error) {
      CDMLogNSError(error, @"reading %@", MPDURL);
      completion(error);
    } else {
      for (Stream *stream in _streams) {
        [self loadStream:stream downloadData:downloadData];
      }
      completion(nil);
    }
  };
  [self downloadAndParseMPD:MPDURL
        withCompletionBlock:wrappedCompletion];
}

// Create Variant playlist that contains all video and audio streams.
- (NSString *)variantPlaylistForStreams:(NSArray<Stream *> *)parsedMPD {
  Stream *stream = nil;
  NSString *playlist = kVariantPlaylist;
  NSString *defaultAudioString = @"NO";
  for (stream in parsedMPD) {
    if (stream.isVideo) {
      NSString *videoString = [NSString stringWithFormat:kVideoPlaylistFormat,
                               stream.bandwidth,
                               stream.codecs,
                               stream.width,
                               stream.height,
                               stream.streamIndex];
      playlist = [playlist stringByAppendingString:videoString];
    } else {
      NSString *langAbbreviation = [[[NSLocale preferredLanguages] firstObject] substringToIndex:2];
      NSString *langString = [langAbbreviation stringByAppendingString:@"_"];
      if ([[stream.sourceURL absoluteString] containsString:langString]) {
        defaultAudioString = @"YES";
      }
      NSString *audioString = [NSString stringWithFormat:kAudioPlaylistFormat,
                               stream.streamIndex,
                               stream.streamIndex,
                               defaultAudioString];
      playlist = [playlist stringByAppendingString:audioString];
      defaultAudioString = @"NO";
    }
  }
  return playlist;
}

// Build playlist based on SegmentBase input. [On-Demand stream]
- (NSString *)buildSegmentBasePlaylist:(Stream *)stream {
  DashToHlsIndex *DASHIndex = stream.DASHIndex;
  uint64_t maxDuration = 0;
  uint64_t timescale = (DASHIndex->index_count > 0) ? DASHIndex->segments[0].timescale : 90000;
  for (uint64_t count = 0; count < DASHIndex->index_count; ++count) {
    if (DASHIndex->segments[count].duration > maxDuration) {
      maxDuration = DASHIndex->segments[count].duration;
    }
  }
  NSMutableString *playlist = [NSMutableString
      stringWithFormat:kPlaylistVOD, 0, (maxDuration / timescale) + 1];
  [playlist appendString:GetKeyUrl(stream.session)];

  for (uint64_t count = 0; count < DASHIndex->index_count; ++count) {
    [playlist appendFormat:stream.isVideo ? kVideoSegmentFormat : kAudioSegmentFormat,
                           ((float)DASHIndex->segments[count].duration / (float)timescale),
                           stream.streamIndex,
                           count,
                           NULL];
  }
  [playlist appendString:kPlaylistVODEnd];
  return playlist;
}

// Build playlist based on SegmentBase input. [On-Demand or Live stream]
- (NSString *)buildSegmentTemplatePlaylist:(Stream *)stream {
  NSUInteger currentSegment = 0;
  NSUInteger endSegment = 0;
  NSUInteger segmentBuffer = 3;
  LiveStream *liveStream = stream.liveStream;
  float currentTime = [_timeProvider getCurrentTime];

  currentSegment = stream.isVideo ? _currentVideoSegment : _currentAudioSegment;

  // Ensure first segment isnt higher than current segment.
  if (currentSegment < liveStream.startNumber) {
    currentSegment = liveStream.startNumber;
  }

  // Set End Segment based on how often the playlist needs to be refreshed.
  if (!liveStream.minimumUpdatePeriod) {
    // Set the amount of segments to retain (default to 3 segments ahead).
    liveStream.minimumUpdatePeriod = liveStream.segmentDuration * segmentBuffer;
  }

  endSegment = (currentTime / liveStream.segmentDuration) + currentSegment +
               segmentBuffer;

  // Check if using duration is known to determine how many segments to create.
  if (stream.mediaPresentationDuration) {
    endSegment = (stream.mediaPresentationDuration / liveStream.segmentDuration);
    endSegment += liveStream.startNumber;
  }

  // Length to keep segments in HLS Playlist.
  if (liveStream.timeShiftBufferDepth) {
    NSUInteger segmentDiff =
        liveStream.timeShiftBufferDepth / liveStream.segmentDuration;
    if ((endSegment - liveStream.startNumber) > segmentDiff) {
      currentSegment = endSegment - segmentDiff;
    }
  }
  // Determine start segment based on the available time of stream and current
  // time.
  if (liveStream.availabilityStartTime) {
    NSDate *now = [NSDate date];
    NSTimeInterval timeDiff = [now timeIntervalSinceDate:liveStream.availabilityStartTime];
    endSegment = timeDiff / liveStream.segmentDuration + liveStream.startNumber;
    if (currentSegment == 0) {
      currentSegment = endSegment - segmentBuffer;
    }
    if (liveStream.timeShiftBufferDepth) {
      int segmentShift = (liveStream.timeShiftBufferDepth / liveStream.segmentDuration);
      currentSegment = currentSegment - segmentShift;
    }
  }

  // Create Playlist by looping through segment list.
  NSMutableString *playlist = nil;
  playlist = [NSMutableString
      stringWithFormat:kDynamicPlaylistHeader, (int)currentSegment,
                       (int)liveStream.segmentDuration + 1];
  [playlist appendString:GetKeyUrl(stream.session)];
  for (uint64_t count = currentSegment; count < endSegment; ++count) {
    [playlist appendFormat:stream.isVideo ? kVideoSegmentFormat : kAudioSegmentFormat,
                           (float)liveStream.segmentDuration, stream.streamIndex, count, NULL];
  }
  // Known length of stream is known. End Playlist.
  if (stream.mediaPresentationDuration) {
    [playlist appendString:kPlaylistVODEnd];
  } else {
    [playlist appendString:kDiscontinuity];
    stream.live = YES;
  }
  return playlist;
}

// Build playlist based on SegmentBase input. [On-Demand or Live stream]
- (NSString *)buildSegmentTimePlaylist:(Stream *)stream {
  LiveStream *liveStream = stream.liveStream;
  NSUInteger currentSegment = 0;
  NSUInteger endSegment = [liveStream.durationArray count];

  // Create Playlist by looping through segment list.
  NSMutableString *playlist = nil;
  playlist = [NSMutableString
              stringWithFormat:kDynamicPlaylistHeader, (int)currentSegment,
              (int)liveStream.segmentDuration + 1];
  [playlist appendString:GetKeyUrl(stream.session)];

  for (NSInteger segment = 0; segment < endSegment; ++segment) {
      NSInteger timestamp = [liveStream.durationArray[segment] integerValue];
      NSInteger duration = 0;
      if (segment == 0) {
          duration = timestamp - liveStream.startNumber;
      } else {
          NSInteger previousTimestamp = [liveStream.durationArray[segment-1] integerValue];
          duration = timestamp - previousTimestamp;
      }
      // TODO(seawardt): Break out into separate method.
      [playlist
       appendFormat:stream.isVideo ? kVideoSegmentFormat : kAudioSegmentFormat,
       (float)duration / (float)liveStream.timescale, stream.streamIndex,
       segment, NULL];
  }
  // Known length of stream is known. End Playlist.
  if (stream.mediaPresentationDuration) {
      [playlist appendString:kPlaylistVODEnd];
  } else {
      [playlist appendString:kDiscontinuity];
      stream.live = YES;
  }
  return playlist;
}

// Creates the TS playlist with segments and durations.
- (NSString *)childPlaylistForStream:(Stream *)stream {
  if (stream.DASHMediaType == DASHMediaTypeSegmentBase) {
    return [self buildSegmentBasePlaylist:stream];
  }
  if (stream.DASHMediaType == DASHMediaTypeSegmentTemplateDuration) {
    return [self buildSegmentTemplatePlaylist:stream];
  }
  if (stream.DASHMediaType == DASHMediaTypeSegmentTemplateTimeline) {
    return [self buildSegmentTimePlaylist:stream];
  }
  CDMLogError(@"Failed to Create Child Playlist from %@", stream.sourceURL);
  return nil;
}

// Downloads requested byte range for the stream.
// Called on _playlistBuilderQ.
- (void)loadStream:(Stream *)stream downloadData:(BOOL)downloadData {
  // Check if Stream is Segment Base and does not have a duration, then Live stream.
  if (stream.DASHMediaType != DASHMediaTypeSegmentBase && !stream.mediaPresentationDuration) {
    stream.live = YES;
  }
  NSURL *requestURL = stream.sourceURL;
  Downloader *downloader = [Downloader sharedInstance];
  // Check if DASH Type is something other than Segment Base.
  if (stream.DASHMediaType != DASHMediaTypeSegmentBase) {
    // Determine if stream has Init segment that contains stream data.
    NSURL *initURL = stream.liveStream.initializationURL;
    if (initURL) {
      requestURL = initURL;
    } else {
      NSString *URLString = [stream.sourceURL absoluteString];
      NSString *number = [NSString stringWithFormat:@"%tu", stream.liveStream.startNumber];
      URLString =
          [URLString stringByReplacingOccurrencesOfString:kLiveRepresentationID
                                               withString:stream.liveStream.representationId];
      URLString = [URLString stringByReplacingOccurrencesOfString:kLiveNumber withString:number];
      requestURL = [[NSURL alloc] initWithString:URLString];
    }
    if (downloadData) {
      NSData *data = [downloader dataDownloadedSynchronouslyFromURL:requestURL
                                                          withRange:stream.initialRange];
      if (data == nil) {
        CDMLogError(@"failed to load data from %@", requestURL);
        return;
      }
      if (![stream setUpTransmuxerWithInitializationData:data]) {
        CDMLogError(@"failed to initialize stream from %@", requestURL);
        return;
      }
    }
    stream.playlist = [self childPlaylistForStream:stream];
  } else {
    if (downloadData) {
      [downloader
          downloadPartialData:requestURL
                    withRange:stream.initialRange
                   completion:^(NSData *data, NSError *connectionError) {
                     dispatch_group_async(_playlistBuilderGroup, _playlistBuilderQ, ^{
                       if (!data) {
                         CDMLogNSError(connectionError, @"downloading %@", requestURL);
                       }
                       if (![stream setUpTransmuxerWithInitializationData:data]) {
                         return;
                       }
                       stream.playlist = [self childPlaylistForStream:stream];
                     });
                   }];
    } else {
      stream.playlist = [self childPlaylistForStream:stream];
    }
  }
}

// Validates that all streams are complete and playback is ready.
- (void)streamReady:(Stream *)stream {
  if (stream) {
    stream.processingDone = YES;
    --_numLoadingStreams;
  } else {
    [[iOSCdm sharedInstance] processPsshKey:nil
                               isOfflineVod:[stream.sourceURL isFileURL]
                            completionBlock:^(NSError *error) {
                              if (error) {
                                CDMLogNSError(error, @"obtaining PSSH key");
                                return;
                              }
                              [stream.playlistBuilder streamReady:stream];
                            }];

    _numLoadingStreams = 0;
  }
  if (_numLoadingStreams == 0 && _playlistBuilderQ) {
    // streamReady is called in the middle of processing a stream.
    // Ensure all streams have been processed before sending to video player.
    dispatch_group_notify(_playlistBuilderGroup, dispatch_get_main_queue(), ^() {
      // Send a notification warning you have finished.
      [[NSNotificationCenter defaultCenter] postNotificationName:kLocalPlaylistReadyNotification
                                                          object:self];
    });
  }
}

- (void) destroy {
  @synchronized(self) {
    _shouldDestroy = YES;
  }
}

// Creates TS segments based on downloading a specific byte range.
- (NSData *)tsDataForIndex:(int)index segment:(int)segment {
  NSData *value = nil;
  @synchronized (self) {
    if (!_shouldDestroy)
      value = [self tsDataForIndexInner:index segment:segment];
  }
  return value;
}

- (NSData *)tsDataForIndexInner:(int)index segment:(int)segment {
  if ((int)_streams.count <= index) {
    return nil;
  }
  Stream *stream = _streams[index];
  NSURL *requestURL = nil;
  NSData *data = nil;
  if (stream.DASHMediaType != DASHMediaTypeSegmentBase) {
    NSString *urlString = [stream.sourceURL absoluteString];
    // Swap Placeholder $RepresentationID$ with variable stored in stream.
    urlString = [urlString stringByReplacingOccurrencesOfString:kLiveRepresentationID
                                                     withString:stream.liveStream.representationId];
    // Check if there is numbered padding to $Number variable.
    if ([urlString containsString:kNumberPlaceholder]) {
      NSRegularExpression *numberRegex =
          [[NSRegularExpression alloc] initWithPattern:kNumberRegexPattern options:0 error:nil];
      urlString = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
      NSTextCheckingResult *numberMatch =
          [numberRegex firstMatchInString:urlString
                                  options:0
                                    range:NSMakeRange(0, urlString.length)];
      if (numberMatch) {
        NSRange formatRange = [numberMatch rangeAtIndex:1];

        NSString *segmentFormat = formatRange.location != NSNotFound
                                      ? [urlString substringWithRange:formatRange]
                                      : kNumberFormat;
        NSString *segmentString = [NSString stringWithFormat:segmentFormat, segment];
        urlString = [urlString stringByReplacingCharactersInRange:numberMatch.range
                                                       withString:segmentString];
      }
    }
    // SegmentTimeline replaces $Time$ placeholder with duration.
    if ([urlString containsString:kTimePlaceholder]) {
      NSString *timestamp = nil;
      // DurationArray is an incremental list. First segment is set by startNumber.
      if (segment == 0) {
        timestamp = [@(stream.liveStream.startNumber) stringValue];
      } else {
        // DurationArray does not include first segment "0", minus 1 gives the correct duration.
        timestamp = stream.liveStream.durationArray[segment];
      }
      urlString = [urlString stringByReplacingOccurrencesOfString:kTimePlaceholder
                                                       withString:timestamp];
    }
    requestURL = [[NSURL alloc] initWithString:urlString];
    data = [[Downloader sharedInstance] dataDownloadedSynchronouslyFromURL:requestURL
                                                                 withRange:stream.initialRange];
  } else {
    requestURL = stream.sourceURL;
    if (!stream.DASHIndex) {
      CDMLogError(@"DASHIndex is empty from %@", requestURL);
      return nil;
    }
    if ((int)stream.DASHIndex->index_count <= segment) {
      CDMLogError(@"segment %d is out of range %u from %@",
                  segment,
                  stream.DASHIndex->index_count,
                  requestURL);
      return nil;
    }
    const auto &segments = stream.DASHIndex->segments[segment];
    NSRange range = NSMakeRange((NSInteger)segments.location, (NSInteger)segments.length);
    data = [[Downloader sharedInstance] dataDownloadedSynchronouslyFromURL:requestURL
                                                                 withRange:range];
  }
  if (data.length == 0) {
    CDMLogError(@"could not initialize session from %@", requestURL);
    return nil;
  }
  NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  if ([responseString containsString:@"NoSuchKey"]) {
    CDMLogError(@"key for %@ not found on Google Storage", requestURL);
    return nil;
  }

  const uint8_t *hlsSegment;
  size_t hlsSize;
  DashToHlsStatus status = Udt_ConvertDash(stream.session, segment, (const uint8_t *)data.bytes,
                                           data.length, &hlsSegment, &hlsSize);
  if (kDashToHlsStatus_OK == status) {
    NSData *responseData = [NSData dataWithBytes:hlsSegment length:hlsSize];
    Udt_ReleaseHlsSegment(stream.session, segment);
    if (stream.isVideo) {
      _currentVideoSegment = segment;
    } else {
      _currentAudioSegment = segment;
    }
    return responseData;
  }
  return nil;
}

@end
