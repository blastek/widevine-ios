// Copyright 2017 Google Inc. All rights reserved.

#import "CdmPlayerHelpers.h"

NSURL *CDMDocumentFileURLForFilename(NSString *filename, NSString *subdirectory) {
  NSURL *documentDirectoryURL =
      [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                             inDomains:NSUserDomainMask][0];
  NSString *relativePath = [subdirectory stringByAppendingPathComponent:filename];
  return [NSURL URLWithString:relativePath relativeToURL:documentDirectoryURL];
}
