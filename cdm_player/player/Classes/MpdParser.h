// Copyright 2015 Google Inc. All rights reserved.

#import "AppDelegate.h"
#import "Stream.h"

@interface MPDParser : NSObject <NSXMLParserDelegate>

// Array of streams found in the XML manifest.
@property(nonatomic, strong) NSMutableArray<Stream *> *streams;

- (instancetype)initWithMPDData:(NSData *)MPDData;

// Remove media files listed in Manifest.
+ (void)deleteFilesInMPD:(NSURL *)MPDURL;
// Being parsing of Manifest by passing URL of streaming content.
+ (NSArray<Stream *> *)streamArrayFromMPD:(PlaylistBuilder *)playlistBuilder
                                  MPDData:(NSData *)MPDData
                                  baseURL:(NSURL *)baseURL
                             storeOffline:(BOOL)storeOffline;

// Convert MPEG Dash formatted duration into seconds.
- (NSUInteger)convertDurationToSeconds:(NSString *)string;
@end
