// Copyright 2015 Google Inc. All rights reserved.

#import <Foundation/Foundation.h>

@class PlaylistBuilder;
@class Stream;

// Handles requests on localhost for an HLS playlist, downloads the corresponding MPEG-DASH
// manifest, and returns a response with a temporary HLS playlist based on the information
// in the mpd.
@interface PlaylistServer : NSObject

// Internal server URL string for streaming locally.
// For example: "http:/localhost:8000/"
@property(nonatomic, strong) NSString *address;
// Holds value if license has been stored offline.
// Determines where to fetch the license.
@property(nonatomic) BOOL offline;

@property(nonatomic, readonly) PlaylistBuilder *builder;

// Init method. isAirplayActive determines what local address to be used when setting up the local
// web server.
- (instancetype)initWithAirplay:(BOOL)isAirplayActive
               licenseServerURL:(NSURL *)licenseServerURL;
// Re-creates the PlaylistServer object.
// Used primarily when switching between AirPlay and non-Airplay usage.
- (void)restart:(BOOL)isAirplayActive;
// Destroys the PlaylistServer object.
- (void)stop;
// XML Parsing of the DASH Manifest that populates the Stream object values.
- (void)processMPD:(NSURL *)MPDURL withCompletion:(void (^)(NSError *))completion;

@end
