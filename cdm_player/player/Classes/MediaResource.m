// Copyright 2015 Google Inc. All rights reserved.

#import "MediaResource.h"

#import "AppDelegate.h"
#import "CdmPlayerErrors.h"
#import "CdmPlayerHelpers.h"
#import "Logging.h"
#import "MpdParser.h"

static NSString *const kJsonMediaName = @"media_name";
static NSString *const kJsonMediaThumbnailURL = @"media_thumbnail_url";
static NSString *const kJsonMediaURL = @"media_url";
static NSString *const kJsonLicenseServerURL = @"license_server_url";
static NSString *const kMpdString = @"mpd";


NSString *const kDownloadedStatusChangedNotification = @"DownloadedStatusChangedNotification";
NSString *kMimeType = @"video/mp4";

static DashToHlsStatus mediaResourcePSSHHandler(void *context,
                                                const uint8_t *PSSH,
                                                size_t PSSH_length) {
  MediaResource *mediaResource = (__bridge MediaResource *)(context);
  NSData *PSSHData = [NSData dataWithBytes:PSSH length:PSSH_length];
  if (mediaResource.releaseLicense) {
    [[iOSCdm sharedInstance] removeOfflineLicenseForPsshKey:PSSHData
                                            completionBlock:^(NSError *error) {
                                              if (error) {
                                                CDMLogNSError(error,
                                                              @"removing license of %@",
                                                              mediaResource.name);
                                              }
                                            }];
    return kDashToHlsStatus_OK;
  }
  if (mediaResource.getLicenseInfo) {
    [[iOSCdm sharedInstance]
     getLicenseInfo:PSSHData
     completionBlock:^(int64_t *expiration, NSError *error) {
       if (error) {
         CDMLogNSError(error, @"retrieving license info for %@", mediaResource.name);
       }
     }];
    return kDashToHlsStatus_OK;
  }
  [[iOSCdm sharedInstance] processPsshKey:PSSHData
                             isOfflineVod:YES
                          completionBlock:^(NSError *error){
                          }];
  return kDashToHlsStatus_OK;
}

DashToHlsStatus mediaResourceDecryptionHandler(void *context,
                                               const uint8_t *encrypted,
                                               uint8_t *clear,
                                               size_t length,
                                               uint8_t *iv,
                                               size_t iv_length,
                                               const uint8_t *key_id,
                                               struct SampleEntry *sampleEntry,
                                               size_t sampleEntrySize) {
  return kDashToHlsStatus_OK;
}

@interface MediaResource ()
- (NSURL *)offlineDirectory;
- (void)fetchPSSHFromFileURL:(NSURL *)fileURL
                initialRange:(NSRange)initialRange
             completionBlock:(MediaResourceCompletionBlock)completionBlock;
@end

@implementation MediaResource {
  dispatch_queue_t _downloadQueue;
  MediaResourceCompletionBlock _storedDownloadCompletionBlock;
  int _lastPercentage;
  CancelToken _cancelToken;
}

- (instancetype)initWithJson:(NSDictionary *)jsonDictionary {
  self = [super init];
  if (!jsonDictionary) {
    return nil;
  }
  if (self) {
    NSString *name = [jsonDictionary objectForKey:kJsonMediaName];
    NSString *licenseServerURL = [jsonDictionary objectForKey:kJsonLicenseServerURL];
    NSString *thumbnailURL = [jsonDictionary objectForKey:kJsonMediaThumbnailURL];
    NSString *URL = [jsonDictionary objectForKey:kJsonMediaURL];
    if (URL && name) {
      _name = [name copy];
      _licenseServerURL = [NSURL URLWithString:licenseServerURL];
      _thumbnailURL = [NSURL URLWithString:thumbnailURL];
      _URL = [NSURL URLWithString:URL];
      _downloadQueue = dispatch_queue_create("com.google.widevine.cdm-player.download", NULL);
      _filesBeingDownloaded = [NSMutableArray array];
      _downloads = [[NSMutableDictionary alloc] init];
      _percentage = 0;
    } else {
      CDMLogWarn(@"Media entry found, but not complete. Skipping %@", jsonDictionary);
      return nil;
    }
  }
  return self;
}

- (void)downloadMediaResourceWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  if (_storedDownloadCompletionBlock || _filesBeingDownloaded.count > 0) {
    // The download is already going on.
    NSError *downloadingError = [NSError cdmErrorWithCode:CdmPlayeriOSErrorCode_AlreadyDownloading
                                                 userInfo:nil];
    completionBlock(downloadingError);
    return;
  }

  // Create the asset directory if it doesn't already exist.
  NSError *error;
  [[NSFileManager defaultManager] createDirectoryAtPath:self.offlineDirectory.path
                            withIntermediateDirectories:NO
                                             attributes:nil
                                                  error:&error];

  if (error) {
    CDMLogNSError(error, @"making asset directory for file \"%@\"", self.URL.lastPathComponent);
    completionBlock(error);
    return;
  }

  // Store the completion block so it can be used once the download is over.
  _storedDownloadCompletionBlock = completionBlock;

  // Start the download.
  _cancelToken = [[Downloader sharedInstance] downloadFromURL:self.URL
                                                  toFileAtURL:self.offlinePath
                                                     delegate:self];
}

- (void)deleteMediaResourceWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  // TODO: this tends to lead to CryptoSession::Terminate: errors, even if it works

  // Delete offline licenses.
  dispatch_group_t deleteGroup = dispatch_group_create();
  NSData *mpdData = [NSData dataWithContentsOfURL:self.offlinePath];
  if (mpdData) {
    NSArray *offlineFiles = [MPDParser streamArrayFromMPD:nil
                                                  MPDData:mpdData
                                                  baseURL:self.URL
                                             storeOffline:YES];
    _releaseLicense = YES;
    for (Stream *stream in offlineFiles) {
      NSString *streamName = stream.sourceURL.lastPathComponent;
      CDMLogInfo(@"Attempting to remove license for stream %@", streamName);
      dispatch_group_enter(deleteGroup);
      // TODO (theodab): refactor the CDM to make key files inside the MPD folder, instead of in
      // a separate folder
      // maybe with a local keymap in the folder
      // this will make deleting the keys a much simpler, more reliable operation, and will make
      // this section of the code unnecessary
      [self fetchPSSHFromFileURL:stream.sourceURL
                    initialRange:stream.initialRange
                 completionBlock:^(NSError *error) {
        if (!error) {
          CDMLogInfo(@"Removed license for stream %@", streamName);
        }
        dispatch_group_leave(deleteGroup);
      }];
    }
  }

  // This simply deletes the entire folder for this media resource.
  dispatch_group_notify(deleteGroup, _downloadQueue, ^{
    _releaseLicense = NO;
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtURL:[self offlineDirectory] error:&error];
    dispatch_async(dispatch_get_main_queue(), ^{
      completionBlock(error);
    });
  });
}


- (void)fetchLicenseInfoWithCompletionBlock:(MediaResourceCompletionBlock)completionBlock {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  CDMLogInfo(@"Fetching license info for %@", self.URL);

  // Set flag to let PSSH Handler pull license information.
  _getLicenseInfo = YES;
  NSArray *offlineFiles =
      [MPDParser streamArrayFromMPD:nil
                            MPDData:[NSData dataWithContentsOfURL:self.URL]
                            baseURL:self.URL
                       storeOffline:YES];
  __block NSError *expirationError = nil;
  dispatch_group_t expirationGroup = dispatch_group_create();
  dispatch_queue_t expirationQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  for (Stream *stream in offlineFiles) {
    dispatch_group_enter(expirationGroup);
    [self fetchPSSHFromFileURL:stream.sourceURL
                  initialRange:stream.initialRange
               completionBlock:^(NSError *error) {
                 expirationError = error;
                 if (error) {
                   CDMLogNSError(error, @"getting expiration date from file");
                   expirationError = error;
                 }
                 dispatch_group_leave(expirationGroup);
               }];
  }

  dispatch_group_notify(expirationGroup, expirationQueue, ^{
    NSError *error = expirationError;
    if (error) {
      dispatch_async(dispatch_get_main_queue(), ^{
        CDMLogNSError(error, @"getting expiration date from file");
        completionBlock(error);
      });
    } else {
      dispatch_async(dispatch_get_main_queue(), ^{
        _getLicenseInfo = NO;
        completionBlock(error);
      });
    }
  });
}

- (void)fetchPSSHFromFileURL:(NSURL *)fileURL
                initialRange:(NSRange)initialRange
             completionBlock:(MediaResourceCompletionBlock)completionBlock {
  CDMLogInfo(@"Fetching PSSH from %@", fileURL);

  Downloader *downloader = [Downloader sharedInstance];
  [downloader downloadPartialData:fileURL
                        withRange:initialRange
                       completion:^(NSData *data, NSError *connectionError) {
                         dispatch_async(_downloadQueue, ^() {
                           if (!data) {
                             completionBlock(connectionError);
                           } else if (![self findPSSH:data]) {
                             CDMPlayerErrorCode errorCode = CdmPlayeriOSErrorCode_CouldNotFindPSSH;
                             completionBlock([NSError cdmErrorWithCode:errorCode userInfo:nil]);
                           } else {
                             completionBlock(nil);
                           }
                         });
                       }];
}

- (BOOL)findPSSH:(NSData *)initializationData {
  NSParameterAssert(initializationData);

  struct DashToHlsSession *session = NULL;
  DashToHlsStatus status = Udt_CreateSession(&session);
  if (status != kDashToHlsStatus_OK) {
    CDMLogError(@"failed to initialize dash to HLS session for %@", self.URL);
    return NO;
  }
  status = DashToHls_SetCenc_PsshHandler(session, (__bridge DashToHlsContext)(self),
                                         mediaResourcePSSHHandler);
  if (status != kDashToHlsStatus_OK) {
    CDMLogError(@"failed to set PSSH handler with URL %@", self.URL);
    return NO;
  }
  status = DashToHls_SetCenc_DecryptSample(session, nil, mediaResourceDecryptionHandler, false);
  if (status != kDashToHlsStatus_OK) {
    CDMLogError(@"failed to set decrypt handler for URL %@", self.URL);
    return NO;
  }
  status = Udt_ParseDash(
      session, 0, (uint8_t *)[initializationData bytes], [initializationData length], nil, 0, nil);
  if (status == kDashToHlsStatus_ClearContent) {
  } else if (status == kDashToHlsStatus_OK) {
  } else {
    CDMLogError(@"failed to parse DASH for session with URL %@", self.URL);
    Udt_ReleaseSession(session);
    return NO;
  }
  Udt_ReleaseSession(session);
  return YES;
}

- (BOOL)isDownloaded {
  NSString *path = [NSString stringWithUTF8String:self.offlinePath.fileSystemRepresentation];
  return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

- (NSURL *)offlineDirectory {
  return self.offlinePath.URLByDeletingLastPathComponent;
}

- (NSURL *)offlinePath {
  return CDMDocumentFileURLForFilename(self.URL.lastPathComponent, self.URL.lastPathComponent);
}

// Internal method. Note that this should be called on the main queue.
- (void)didFinishDownloadingFileWithURL:(NSURL *)fileURL {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  [_filesBeingDownloaded removeObject:fileURL];
  if (_filesBeingDownloaded.count == 0) {
    NSString *notificationName = kDownloadedStatusChangedNotification;
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self];

    // Call and discard the stored completion block.
    MediaResourceCompletionBlock completionBlock = _storedDownloadCompletionBlock;
    _storedDownloadCompletionBlock = nil;
    completionBlock(nil);
  }
}

#pragma mark - downloader delegate methods

- (void)downloader:(Downloader *)downloader
    didStartDownloadingFromURL:(NSURL *)sourceURL
                   toFileAtURL:(NSURL *)fileURL {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  _percentage = 0;
  _lastPercentage = -1;
  [_filesBeingDownloaded addObject:fileURL];
}

- (void)downloader:(Downloader *)downloader
    didFailToDownloadFromURL:(NSURL *)sourceURL
                   withError:(NSError *)error {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  CDMLogNSError(error, @"downloading %@", sourceURL);

  // Delete any existing files.
  [self deleteMediaResourceWithCompletionBlock:^(NSError *error) {
    // This callback is called on the main queue.
    if (_storedDownloadCompletionBlock) {
      // Don't do anything if the download completion block doesn't exist
      // because multiple failures in a row are irrelevant.

      // Cancel the download.
      [[Downloader sharedInstance] cancelDownloadWithCancelToken:_cancelToken];

      // Discard all of the data about downloads.
      [_filesBeingDownloaded removeAllObjects];
      _percentage = 0;

      NSString *notificationName = kDownloadedStatusChangedNotification;
      [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self];

      // Call and discard the stored completion block.
      _storedDownloadCompletionBlock(error);
      _storedDownloadCompletionBlock = nil;
    }
  }];
}

- (void)downloader:(Downloader *)downloader
    didFinishDownloadingFromURL:(NSURL *)sourceURL
                    toFileAtURL:(NSURL *)fileURL {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  CDMLogInfo(@"Finished downloading %@", fileURL);
  if (_storedDownloadCompletionBlock == nil) {
    // You aren't downloading anymore; this only happens if you have already failed to download
    // something, and it didn't cancel in time (i.e. another download succeeded while the
    // resource was waiting to enter the synchronize block in downloadFailedForURL.)
    // So just discard this.
    CDMLogInfo(@"Discarded %@ due to previous download failure.", fileURL);
    [self deleteMediaResourceWithCompletionBlock:^(NSError *error) {
      [_filesBeingDownloaded removeObject:fileURL];
    }];
  } else if (![fileURL.pathExtension isEqualToString:@"mpd"]) {
    [self fetchPSSHFromFileURL:fileURL
                  initialRange:NSMakeRange(0, NSUIntegerMax)
               completionBlock:^(NSError *error) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                   // A media resource isn't 'truly' downloaded until its PSSH has been read.
                   // This way, the validity of a downloaded resource can be identified before
                   // calling the file 'downloaded'.
                   if (error) {
                     [self downloader:downloader
             didFailToDownloadFromURL:sourceURL
                            withError:error];
                   } else if (_storedDownloadCompletionBlock) {
                     [self didFinishDownloadingFileWithURL:fileURL];
                   } else {
                     // As above; a download failed already.
                     CDMLogInfo(@"Discarded %@ due to previous download failure.", fileURL);
                     [self deleteMediaResourceWithCompletionBlock:^(NSError *error) {
                       [_filesBeingDownloaded removeObject:fileURL];
                     }];
                   }
                 });
               }];
  } else {
    [self didFinishDownloadingFileWithURL:fileURL];
  }
}

- (void)downloader:(Downloader *)downloader
    didUpdateDownloadProgress:(float)progress
                 forFileAtURL:(NSURL *)fileURL {
  // This method must be called on the main queue.
  assert([NSThread isMainThread]);

  if (_storedDownloadCompletionBlock == nil) {
    // The download has failed. Ignore the update.
    return;
  }

  float totalDownloadProgress = 0.f;
  if (![fileURL.pathExtension isEqualToString:kMpdString]) {
    _downloads[fileURL] = @(progress);
    for (NSNumber *progress in _downloads.allValues) {
      totalDownloadProgress += progress.floatValue;
    }
  }
  _percentage = (totalDownloadProgress / _downloads.count) * 100;
  if (_percentage != _lastPercentage) {
    NSString *notificationName = kDownloadedStatusChangedNotification;
    _lastPercentage = _percentage;
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                        object:self];
  }
}

@end
