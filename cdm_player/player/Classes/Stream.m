// Copyright 2015 Google Inc. All rights reserved.

#import "Stream.h"

#import "LiveStream.h"
#import "PlaylistBuilder.h"
#import "Logging.h"

static NSString *kAudioMimeType = @"audio/mp4";
static NSString *kVideoMimeType = @"video/mp4";

// Handler used to hold pass the PSSH (License Key) to the DASH Transmuxer as part of
// Udt_SetPSSHHandler.
static DashToHlsStatus dashPSSHHandler(void *context, const uint8_t *PSSH,
                                       size_t PSSH_length) {
  NSData *PSSHData = [NSData dataWithBytes:PSSH length:PSSH_length];
  Stream *stream = (__bridge Stream *)(context);
  // TODO(seawardt@): Change processPsshKey to processPSSHKey upstream in Host. (ObjC style)
  [[iOSCdm sharedInstance] processPsshKey:PSSHData
                             isOfflineVod:[stream.sourceURL isFileURL]
                          completionBlock:^(NSError *error) {
                            if (error) {
                              CDMLogNSError(error, @"obtaining PSSH key");
                              return;
                            }
                            [stream.playlistBuilder streamReady:stream];
                          }];
  return kDashToHlsStatus_OK;
}

// Handler to be used with Udt_SetDecryptSample from the DASH Transmuxer.
static DashToHlsStatus dashDecryptionHandler(void *context,
                                             const uint8_t *encrypted,
                                             uint8_t *clear,
                                             size_t length,
                                             uint8_t *iv,
                                             size_t iv_length,
                                             const uint8_t *key_id,
                                             struct SampleEntry *sampleEntry,
                                             size_t sampleEntrySize) {
  NSData *decrypted =
      [[iOSCdm sharedInstance] decrypt:[NSData dataWithBytes:encrypted length:length]
                                 keyId:[NSData dataWithBytes:key_id length:16]
                                    IV:[NSData dataWithBytes:iv length:iv_length]];
  if (!decrypted) {
    return kDashToHlsStatus_BadDashContents;
  }
  memcpy(clear, [decrypted bytes], length);
  return kDashToHlsStatus_OK;
}

@implementation LiveStream
@end

@implementation Stream

- (id)initWithPlaylistBuilder:(PlaylistBuilder *)playlistBuilder {
  self = [super init];
  if (self) {
    _liveStream = [[LiveStream alloc] init];
    _playlistBuilder = playlistBuilder;
  }
  return self;
}

- (BOOL)setUpTransmuxerWithInitializationData:(nonnull NSData *)initializationData {
  struct DashToHlsSession *session = NULL;
  DashToHlsStatus status = Udt_CreateSession(&session);
  if (status != kDashToHlsStatus_OK || !session) {
    CDMLogError(@"failed to initialize session");
    return NO;
  }
  _session = session;
  status = [self setPSSHHandler:dashPSSHHandler];
  if (status != kDashToHlsStatus_OK) {
    CDMLogError(@"failed to set PSSH handler");
    return NO;
  }
  status = [self setDecryptionHandler:dashDecryptionHandler];
  if (status != kDashToHlsStatus_OK) {
    CDMLogError(@"failed to set decrypt handler");
    return NO;
  }
  status = [self parseInitializationData:initializationData];
  if (status == kDashToHlsStatus_ClearContent) {
    if (_playlistBuilder) {
      [_playlistBuilder streamReady:self];
    } else {
      return NO;
    }
  } else if (status == kDashToHlsStatus_OK) {
  } else {
    CDMLogError(@"failed to parse dash");
    Udt_PrettyPrint(_session);
    return NO;
  }
  return YES;
}

- (DashToHlsStatus)setPSSHHandler:(DashToHlsContext)handler {
  if (handler && _session) {
    return DashToHls_SetCenc_PsshHandler(
        _session, (__bridge DashToHlsContext)(self), handler);
  }
  return kDashToHlsStatus_BadConfiguration;
}

- (DashToHlsStatus)setDecryptionHandler:(DashToHlsContext)handler {
  if (handler && _session) {
    return DashToHls_SetCenc_DecryptSample(
        _session, (__bridge DashToHlsContext)(self), handler, false);
  }
  return kDashToHlsStatus_BadConfiguration;
}

- (DashToHlsStatus)parseInitializationData:(NSData *)initializationData {
  // If SegmentBase, use 0 to pass as Stream Index.
  return Udt_ParseDash(
      _session, _DASHMediaType == DASHMediaTypeSegmentBase ? 0 : _streamIndex,
      (uint8_t *)[initializationData bytes], [initializationData length], (uint8_t *)[_PSSH bytes],
      [_PSSH length], &_DASHIndex);
}

// Debug logging format.
- (NSString *)description {
  return
      [NSString stringWithFormat:
                    @"Stream%lu: isVideo=%s codec=%@ bandwidth=%lu \n URL=%@",
                    (unsigned long)_streamIndex, _video ? "YES" : "NO", _codecs,
                    (unsigned long)_bandwidth, _sourceURL];
}

@end
