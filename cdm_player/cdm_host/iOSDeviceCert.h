// Copyright 2015 Google Inc. All Rights Reserved.

#ifndef WIDEVINE_BASE_CDM_HOST_IOS_IOSDEVICECERT_H_
#define WIDEVINE_BASE_CDM_HOST_IOS_IOSDEVICECERT_H_

#include <stddef.h>
#include <stdint.h>

extern const uint8_t kDeviceCert[];
extern const size_t kDeviceCertSize;

#endif // WIDEVINE_BASE_CDM_HOST_IOS_IOSDEVICECERT_H_
