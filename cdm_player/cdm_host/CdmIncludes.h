// Copyright 2015 Google Inc. All rights reserved.
// Handles the importing of CDM headers based on OEMCrypto Library type.

#ifndef IPHONE_WIDEVINE_CDM_HOST_IOS_CDMINCLUDES_H_
#define IPHONE_WIDEVINE_CDM_HOST_IOS_CDMINCLUDES_H_

# import "cdm.h"

#endif  // IPHONE_WIDEVINE_CDM_HOST_IOS_CDMINCLUDES_H_

